﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace A16_Ex02_Anton_305939050_Daniel_305698912
{
    public static class Program
    {// $G$ THE-001 (-5) your grade on diagrams document - 69. please see comments inside the document. (40% of your grade).
     // $G$ DSN-999 (-5) You should separate the logic and user interface into different classes.

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormFacebookMain());
        }
    }
}
