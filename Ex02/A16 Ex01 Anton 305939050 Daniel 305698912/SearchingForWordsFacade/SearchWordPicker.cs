﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FacebookWrapper.ObjectModel;

namespace A16_Ex02_Anton_305939050_Daniel_305698912.SearchingForWordsFacade
{
    public class SearchWordPicker
    {
        private readonly SearchForWordsByFilter r_SearchForWordsByFilter;

        public SearchWordPicker()
        {
            r_SearchForWordsByFilter = new SearchForWordsByFilter();
        }

        public void SearchWords(FormFacebookMain i_FormFacebookMain)
        {
            if (i_FormFacebookMain.tabPosts.Visible)
            {
                FacebookObjectCollection<Post> wordCollectionAfterSearchInPosts = new FacebookObjectCollection<Post>();

                foreach (Post facebookPost in LoggedInUser.Instance.CurrentUser.Posts)
                {
                    if (SearchForWordsByFilter.SearchWordInPosts(
                        facebookPost,
                        i_FormFacebookMain.textBoxSearch.Text,
                        i_FormFacebookMain.dateTimePickerForSearchStartDate.Text,
                        i_FormFacebookMain.dateTimePickerForSearchEndDate.Text,
                        i_FormFacebookMain.IsSearchByDateEnabled))
                    {
                        wordCollectionAfterSearchInPosts.Add(facebookPost);
                    }
                }

                i_FormFacebookMain.postBindingSource.DataSource = wordCollectionAfterSearchInPosts;
            }
            else if (i_FormFacebookMain.tabEvents.Visible)
            {
                FacebookObjectCollection<Event> wordCollectionAfterSearchInEvents = new FacebookObjectCollection<Event>();

                foreach (Event facbookEvent in LoggedInUser.Instance.CurrentUser.Events)
                {
                    if (SearchForWordsByFilter.SearchWordsInEvents(facbookEvent,
                        i_FormFacebookMain.textBoxSearch.Text,
                        i_FormFacebookMain.dateTimePickerForSearchStartDate.Text,
                        i_FormFacebookMain.dateTimePickerForSearchEndDate.Text,
                        i_FormFacebookMain.IsSearchByDateEnabled))
                    {
                        wordCollectionAfterSearchInEvents.Add(facbookEvent);
                    }
                }

                i_FormFacebookMain.eventsBindingSource.DataSource = wordCollectionAfterSearchInEvents;
            }
            else if (i_FormFacebookMain.tabCheckIn.Visible)
            {
                FacebookObjectCollection<Checkin> wordCollectionAfterSearchInCheckIns = new FacebookObjectCollection<Checkin>();

                foreach (Checkin facebookCheckin in LoggedInUser.Instance.CurrentUser.Checkins)
                {
                    if (SearchForWordsByFilter.SearchWordsInCheckIn(facebookCheckin,
                        i_FormFacebookMain.textBoxSearch.Text,
                        i_FormFacebookMain.dateTimePickerForSearchStartDate.Text,
                        i_FormFacebookMain.dateTimePickerForSearchEndDate.Text,
                        i_FormFacebookMain.IsSearchByDateEnabled))
                    {
                        wordCollectionAfterSearchInCheckIns.Add(facebookCheckin);
                    }
                }

                i_FormFacebookMain.checkinsBindingSource.DataSource = wordCollectionAfterSearchInCheckIns;
            }
            else if (i_FormFacebookMain.tabFriends.Visible)
            {
                FacebookObjectCollection<User> wordCollectionAfterSearchInFriends = new FacebookObjectCollection<User>();

                foreach (User facebookFriend in LoggedInUser.Instance.CurrentUser.Friends)
                {
                    if (SearchForWordsByFilter.SearchWordsInFriends(facebookFriend,
                        i_FormFacebookMain.textBoxSearch.Text,
                        i_FormFacebookMain.dateTimePickerForSearchStartDate.Text,
                        i_FormFacebookMain.dateTimePickerForSearchEndDate.Text,
                        i_FormFacebookMain.IsSearchByDateEnabled))
                    {
                        wordCollectionAfterSearchInFriends.Add(facebookFriend);
                    }
                }

                i_FormFacebookMain.userBindingSource.DataSource = wordCollectionAfterSearchInFriends;
            }
        }

        private SearchForWordsByFilter SearchForWordsByFilter
        {
            get { return r_SearchForWordsByFilter; }
        }
    }
}
