﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FacebookWrapper.ObjectModel;

namespace A16_Ex02_Anton_305939050_Daniel_305698912
{
    public class SearchForWordsByFilter
    {
        public bool SearchWordInPosts(Post i_CurrentFacebookPost, string i_WordForSearch, string i_FacebookDateTimeShortStartChoosedByUser, string i_FacebookDateTimeShortEndChoosedByUser, bool i_IsSearchByDateEnabled)
        {
            DateTime dateFormat;
            bool isSearchSucceded = false;

            if (!string.IsNullOrEmpty(i_CurrentFacebookPost.Message) && i_CurrentFacebookPost.Message.Contains(i_WordForSearch))
            {
                isSearchSucceded = true;

                if (i_IsSearchByDateEnabled)
                {
                    try
                    {
                        if (DateTime.TryParse(i_CurrentFacebookPost.UpdateTime.ToString(), out dateFormat))
                        {
                            isSearchSucceded = searchByDate(
                                dateFormat,
                                i_FacebookDateTimeShortStartChoosedByUser,
                                i_FacebookDateTimeShortEndChoosedByUser);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }

            return isSearchSucceded;
        }

        public bool SearchWordsInEvents(Event i_CurrentFacebookEvent, string i_WordForSearch, string i_FacebookDateTimeShortStartChoosedByUser, string i_FacebookDateTimeShortEndChoosedByUser, bool i_IsSearchByDateEnabled)
        {
            DateTime dateFormat;
            bool isSearchSucceded = false;

            if (!string.IsNullOrEmpty(i_CurrentFacebookEvent.Name) && i_CurrentFacebookEvent.Name.Contains(i_WordForSearch))
            {
                isSearchSucceded = true;

                if (i_IsSearchByDateEnabled)
                {
                    try
                    {
                        if (DateTime.TryParse(i_CurrentFacebookEvent.StartTime.ToString(), out dateFormat))
                        {
                            isSearchSucceded = searchByDate(
                                dateFormat,
                                i_FacebookDateTimeShortStartChoosedByUser,
                                i_FacebookDateTimeShortEndChoosedByUser);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }

            return isSearchSucceded;
        }

        public bool SearchWordsInCheckIn(Checkin i_CurrentFacebookCheckIn, string i_WordForSearch, string i_FacebookDateTimeShortStartChoosedByUser, string i_FacebookDateTimeShortEndChoosedByUser, bool i_IsSearchByDateEnabled)
        {
            DateTime dateFormat;
            bool isSearchSucceded = false;

            if (!string.IsNullOrEmpty(i_CurrentFacebookCheckIn.Place.Name) && i_CurrentFacebookCheckIn.Place.Name.Contains(i_WordForSearch))
            {
                isSearchSucceded = true;

                if (i_IsSearchByDateEnabled)
                {
                    try
                    {
                        if (DateTime.TryParse(i_CurrentFacebookCheckIn.UpdateTime.ToString(), out dateFormat))
                        {
                            isSearchSucceded = searchByDate(
                                dateFormat,
                                i_FacebookDateTimeShortStartChoosedByUser,
                                i_FacebookDateTimeShortEndChoosedByUser);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }

            return isSearchSucceded;
        }

        public bool SearchWordsInFriends(User i_CurrentFacebookFriend, string i_WordForSearch, string i_FacebookDateTimeShortStartChoosedByUser, string i_FacebookDateTimeShortEndChoosedByUser, bool i_IsSearchByDateEnabled)
        {
            DateTime dateFormat;
            bool isSearchSucceded = false;

            if (!string.IsNullOrEmpty(i_CurrentFacebookFriend.Name) && i_CurrentFacebookFriend.Name.Contains(i_WordForSearch))
            {
                isSearchSucceded = true;

                if (i_IsSearchByDateEnabled)
                {
                    try
                    {
                        if (DateTime.TryParse(i_CurrentFacebookFriend.UpdateTime.ToString(), out dateFormat))
                        {
                            isSearchSucceded = searchByDate(
                                dateFormat,
                                i_FacebookDateTimeShortStartChoosedByUser,
                                i_FacebookDateTimeShortEndChoosedByUser);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            }

            return isSearchSucceded;
        }

        private bool searchByDate(DateTime i_FaceBookDateTimeProperty, string i_FacebookDateTimeShortStartChoosedByUser, string i_FacebookDateTimeShortEndChoosedByUser)
        {
            bool isSearchSucceded = false;
            DateTime facebookDateTimeShortStartChoosedByUser = new DateTime();
            DateTime facebookDateTimeShortEndChoosedByUser = new DateTime();
            DateTime facebookDateTimeProperty = new DateTime();

            if (DateTime.TryParse(i_FaceBookDateTimeProperty.ToShortDateString(), out facebookDateTimeProperty))
            {
                if (DateTime.TryParse(
                    i_FacebookDateTimeShortStartChoosedByUser,
                    out facebookDateTimeShortStartChoosedByUser)
                    && DateTime.TryParse(
                    i_FacebookDateTimeShortEndChoosedByUser,
                    out facebookDateTimeShortEndChoosedByUser))
                {
                    if (facebookDateTimeProperty >= facebookDateTimeShortStartChoosedByUser
                        && facebookDateTimeProperty <= facebookDateTimeShortEndChoosedByUser)
                    {
                        isSearchSucceded = true;
                    }
                    else
                    {
                        isSearchSucceded = false;
                    }
                }
                else
                {
                    throw new Exception("You choose wrong date");
                }
            }
            else
            {
                throw new Exception("Date is facebook app at property is not correct");
            }

            return isSearchSucceded;
        }
    }
}