﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using A16_Ex02_Anton_305939050_Daniel_305698912.SearchingForWordsFacade;
using FacebookWrapper.ObjectModel;
using FacebookWrapper;

namespace A16_Ex02_Anton_305939050_Daniel_305698912
{
    public partial class FormFacebookMain : Form
    {
        public enum eAccountStatus
        {
            Loggedin = 1,
            Loggedout
        }

        private const string k_DefaultPost = "Please Enter Your Post (Optional)";
        private readonly bool r_IsCheckedOrEnabled = true;
        private static readonly object sr_TabChangeContext = new object();
        private FormPictureFilter m_FormPictureFilter;
        private static eAccountStatus s_AccountLoggedIn;
        private bool m_IsSearchByDateEnabled = false;

        public SearchWordPicker SearchWordPicker { get; set; }

        public static eAccountStatus AccountLoggedIn
        {
            get { return s_AccountLoggedIn; }
            set { s_AccountLoggedIn = value; }
        }

        private bool isCheckedOrEnabled
        {
            get { return r_IsCheckedOrEnabled; }
        }

        public FormFacebookMain()
        {
            InitializeComponent();
            AccountLoggedIn = eAccountStatus.Loggedout;
        }

        public FormPictureFilter FormPictureFilter
        {
            get { return m_FormPictureFilter; }
            set { m_FormPictureFilter = value; }
        }

        public bool IsSearchByDateEnabled
        {
            get { return m_IsSearchByDateEnabled; }
            set { m_IsSearchByDateEnabled = value; }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.Location = AppConfig.Instance.WindowLocation;
            this.checkBoxAutoConnect.Checked = AppConfig.Instance.AutoConnect;

            if (AppConfig.Instance.AutoConnect)
            {
                try
                {
                    connect();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format(
@"{0}
{1}",
                        ex.Message,
                        ex.StackTrace));
                }
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            AppConfig.Instance.WindowLocation = this.Location;
            AppConfig.Instance.AutoConnect = this.checkBoxAutoConnect.Checked;
            AppConfig.Instance.SaveToFile();

            if (m_FormPictureFilter != null && m_FormPictureFilter.Visible)
            {
                m_FormPictureFilter.Invoke(new Action(() => m_FormPictureFilter.Close()));
            }

            base.OnClosing(e);
        }

        private void buttonLoginOrLogout_Click(object sender, EventArgs e)
        {
            if (AccountLoggedIn == eAccountStatus.Loggedout)
            {
                loginAndInit();
            }
            else
            {
                logout();
            }
        }

        private void loginAndInit()
        {
            new Thread(() =>
            {
                this.Invoke(new Action(() =>
                {
                    try
                    {
                        LoggedInUser.LoginAndInit();
                        getUserInfo();
                    }
                    catch (Exception ex)
                    {
                        messageToUser(ex.Message);
                    }
                }));
            }).Start();
        }

        private void logout()
        {
            LoggedInUser.Logout();
            AccountLoggedIn = eAccountStatus.Loggedout;
            resetPropertiesForm();
            buttonLogin.Text = "Login";
            checkBoxAutoConnect.Checked = false;
            messageToUser("You have been disconected");
        }

        private void resetPropertiesForm()
        {
            setButtonsAndBoxsPressedEnbaledState(!isCheckedOrEnabled);
            clearBoxsInformation();
        }

        private void setButtonsAndBoxsPressedEnbaledState(bool i_IsPressedOrEnabled)
        {
            buttonPictureFilter.Enabled = i_IsPressedOrEnabled;
            buttonConnect.Enabled = !i_IsPressedOrEnabled;
            buttonReset.Enabled = i_IsPressedOrEnabled;
            buttonPostOnFacebook.Enabled = i_IsPressedOrEnabled;

            checkBoxAutoConnect.Enabled = i_IsPressedOrEnabled;

            richTextBoxPostOnFacebook.Enabled = i_IsPressedOrEnabled;

            textBoxSearch.Enabled = i_IsPressedOrEnabled;

            checkBoxIfDateWasChecked.Enabled = i_IsPressedOrEnabled;
        }

        private void clearBoxsInformation()
        {
            AccountLoggedIn = eAccountStatus.Loggedout;

            userProfilePictureBox.Image = userProfilePictureBox.ErrorImage;

            this.labelUsername.Text = string.Empty;
            this.labelBirthday.Text = string.Empty;
            this.labelGender.Text = string.Empty;

            richTextBoxPostOnFacebook.Text = k_DefaultPost;
            textBoxSearch.Text = string.Empty;

            dataGridViewCheckIn.Rows.Clear();
            dataGridViewEvents.Rows.Clear();
            dataGridViewPost.Rows.Clear();
            dataGridViewFriends.Rows.Clear();
        }

        private void connect()
        {
            if (AccountLoggedIn == eAccountStatus.Loggedin)
            {
                messageToUser("You are already connected to facebook");
            }
            else if (string.IsNullOrEmpty(AppConfig.Instance.AccessToken))
            {
                MessageBox.Show("No Access Token Available", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    Thread thread = new Thread(() =>
                    {
                        LoggedInUser.Connect();
                        getUserInfo();
                    });

                    thread.IsBackground = true;
                    thread.Start();
                }
                catch (Exception ex)
                {
                    messageToUser(ex.Message);
                }
            }
        }

        private void getUserInfo()
        {
            this.Invoke(new Action(() =>
            {
                try
                {
                    User currentUser = LoggedInUser.Instance.CurrentUser;
                    userBindingSource.DataSource = currentUser;
                    this.userProfilePictureBox.LoadAsync(LoggedInUser.Instance.CurrentUser.PictureLargeURL);
                    this.labelUsername.Text = "Name: " + LoggedInUser.Instance.CurrentUser.Name;
                    this.labelBirthday.Text = "Birthday: " + LoggedInUser.Instance.CurrentUser.Birthday;
                    this.labelGender.Text = "Gender: " + LoggedInUser.Instance.CurrentUser.Gender;

                    buttonLogin.Text = "Logout";
                    AccountLoggedIn = eAccountStatus.Loggedin;
                    setButtonsAndBoxsPressedEnbaledState(isCheckedOrEnabled);

                    getEventsDetails();
                    getCheckInDetails();
                    getFriendList();
                    getProfilePosts();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }));
        }

        private void getFriendList()
        {
            new Thread(() =>
            {
                this.Invoke(new Action(() =>
                {
                    var allFriends = LoggedInUser.Instance.CurrentUser.Friends;

                    if (!dataGridViewFriends.InvokeRequired)
                    {
                        userBindingSource.DataSource = allFriends;

                        pictureBoxFriends.Visible = true;
                        pictureBoxEvents.Visible = false;
                    }
                    else
                    {
                        dataGridViewFriends.Invoke(new Action(() =>
                        {
                            userBindingSource.DataSource = allFriends;
                            pictureBoxFriends.Visible = true;
                        }));
                    }
                }));
            }).Start();
        }

        private void getCheckInDetails()
        {
            new Thread(() =>
            {
                this.Invoke(new Action(() =>
                {
                    var allCheckIns = LoggedInUser.Instance.CurrentUser.Checkins;

                    if (!dataGridViewCheckIn.InvokeRequired)
                    {
                        checkinsBindingSource.DataSource = allCheckIns;

                        pictureBoxFriends.Visible = false;
                        pictureBoxEvents.Visible = false;
                    }
                    else
                    {
                        dataGridViewCheckIn.Invoke(new Action(() => checkinsBindingSource.DataSource = allCheckIns));
                    }
                }
                    ));
            }).Start();
        }

        private void getEventsDetails()
        {
            new Thread(() =>
            {
                this.Invoke(new Action(() =>
                {
                    var allEvents = LoggedInUser.Instance.CurrentUser.Events;

                    if (!dataGridViewEvents.InvokeRequired)
                    {
                        eventsBindingSource.DataSource = allEvents;

                        pictureBoxFriends.Visible = false;
                        pictureBoxEvents.Visible = true;
                    }
                    else
                    {
                        dataGridViewEvents.Invoke(new Action(() => eventsBindingSource.DataSource = allEvents));
                    }
                }));
            }).Start();
        }

        private void getProfilePosts()
        {
            new Thread(() =>
            {
                this.Invoke(new Action(() =>
                {
                    var allWallPosts = LoggedInUser.Instance.CurrentUser.Posts;

                    pictureBoxFriends.Visible = false;
                    pictureBoxEvents.Visible = false;

                    if (!dataGridViewPost.InvokeRequired)
                    {
                        postBindingSource.DataSource = allWallPosts;
                    }
                    else
                    {
                        dataGridViewPost.Invoke(new Action(() => postBindingSource.DataSource = allWallPosts));
                    }
                }));
            }).Start();
        }

        private void userProfileInfo_Enter(object sender, EventArgs e)
        {
        }

        private void formFacebookMain_Load(object sender, EventArgs e)
        {
        }

        private void uploadPost()
        {
            try
            {
                if (!string.IsNullOrEmpty(richTextBoxPostOnFacebook.Text))
                {
                    LoggedInUser.Instance.CurrentUser.PostStatus(richTextBoxPostOnFacebook.Text);
                    messageToUser("Post posted.");
                }
                else
                {
                    messageToUser("Can't post empty message.");
                }
            }
            catch (Exception)
            {
                messageToUser("Your post is not on Facebook.");
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            connect();
        }

        private void buttonPostOnFacebook_Click(object sender, EventArgs e)
        {
            uploadPost();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            checkBoxIfDateWasChecked.Checked = false;
            textBoxSearch.Text = string.Empty;
            richTextBoxPostOnFacebook.Text = k_DefaultPost;

            getEventsDetails();
            getCheckInDetails();
            getFriendList();
            getProfilePosts();
        }

        private void dataGridViewPost_SelectionChanged(object sender, EventArgs e)
        {
            pictureBoxFriends.Visible = false;
            pictureBoxEvents.Visible = false;
        }

        private void dataGridViewEvents_SelectionChanged(object sender, EventArgs e)
        {
            pictureBoxFriends.Visible = false;
            pictureBoxEvents.Visible = true;
        }

        private void dataGridViewCheckIn_SelectionChanged(object sender, EventArgs e)
        {
            pictureBoxFriends.Visible = false;
            pictureBoxEvents.Visible = false;
        }

        private void dataGridViewFriends_SelectionChanged(object sender, EventArgs e)
        {
            pictureBoxFriends.Visible = true;
            pictureBoxEvents.Visible = false;
        }

        private void textBoxSearchOrPost_TextChanged(object sender, EventArgs e)
        {
            searchWords();
        }

        private void FormFacebookMain_MouseClick(object sender, MouseEventArgs e)
        {
            richTextBoxPostOnFacebook.Text = k_DefaultPost;
        }

        private void buttonPictureFilter_Click(object sender, EventArgs e)
        {
            FormPictureFilter = new FormPictureFilter();
            FormPictureFilter.ShowDialog();
        }

        private void richTextBoxPostOnFacebook_MouseDown(object sender, EventArgs e)
        {
            richTextBoxPostOnFacebook.Text = string.Empty;
        }

        private void checkBoxIfDateWasChecked_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxIfDateWasChecked.Checked)
            {
                IsSearchByDateEnabled = true;
                dateTimePickerForSearchStartDate.Enabled = true;
                dateTimePickerForSearchEndDate.Enabled = true;
                
                searchWords();
            }
            else
            {
                IsSearchByDateEnabled = false;
                dateTimePickerForSearchStartDate.Enabled = false;
                dateTimePickerForSearchEndDate.Enabled = false;

                searchWords();
            }
        }
        
        private void messageToUser(string i_MessageToUser)
        {
            MessageBox.Show(i_MessageToUser);
        }

        private void searchWords()
        {
            SearchWordPicker = new SearchWordPicker();
            SearchWordPicker.SearchWords(this);
        }

        private void dateTimePickerForSearchStartDate_ValueChanged(object sender, EventArgs e)
        {
            searchWords();
        }

        private void dateTimePickerForSearchEndDate_ValueChanged(object sender, EventArgs e)
        {
            searchWords();
        }

        private void tabControlUserInfo_Selected_1(object sender, TabControlEventArgs e)
        {
            dataGridViewPost_SelectionChanged(sender, EventArgs.Empty);
        }
    }
}