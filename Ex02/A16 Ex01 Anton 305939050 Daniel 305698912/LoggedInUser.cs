﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FacebookWrapper;
using FacebookWrapper.ObjectModel;

namespace A16_Ex02_Anton_305939050_Daniel_305698912
{
    public class LoggedInUser
    {
        private static readonly string sr_AppID = "772224062883316";
        private static readonly object sr_CreationContext = new object();
        private static LoggedInUser s_Instance;
        private static User s_CurrentֻֻֻֻֻֻֻֻֻUser;

        private LoggedInUser()
        {

        }

        public static string AppID
        {
            get { return sr_AppID; }
        }

        public static LoggedInUser Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    lock (sr_CreationContext)
                    {
                        if (s_Instance == null)
                        {
                            s_Instance = new LoggedInUser();
                        }
                    }
                }

                return s_Instance;
            }
        }

        public User CurrentUser
        {
            get { return s_CurrentֻֻֻֻֻֻֻֻֻUser; }
        }

        public static void Connect()
        {
            try
            {
                LoginResult result = FacebookService.Connect(AppConfig.Instance.AccessToken);

                s_CurrentֻֻֻֻֻֻֻֻֻUser = result.LoggedInUser;
            }
            catch (Exception)
            {
                LoginAndInit();
            }
        }

        public static void LoginAndInit()
        {
            try
            {
                LoginResult result = FacebookService.Login(
                    AppID,
                    "user_about_me",
                    "user_friends",
                    "user_posts",
                    "user_events",
                    "user_photos",
                    "publish_stream",
                    "publish_actions",
                    "user_status");

                s_CurrentֻֻֻֻֻֻֻֻֻUser = result.LoggedInUser;
                AppConfig.Instance.AccessToken = result.AccessToken;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void Logout()
        {
            FacebookService.Logout(null);
            s_CurrentֻֻֻֻֻֻֻֻֻUser = null;
        }
    }
}
