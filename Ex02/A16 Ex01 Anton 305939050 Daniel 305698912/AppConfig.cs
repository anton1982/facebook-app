﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using A16_Ex02_Anton_305939050_Daniel_305698912;

namespace A16_Ex02_Anton_305939050_Daniel_305698912
{
    public class AppConfig
    {
        private static AppConfig s_AppConfig;
        private static readonly Object sr_GetOrCreateContext = new object();

        private AppConfig()
        {
            
        }

        public static AppConfig Instance
        {
            get
            {
                if (s_AppConfig == null)
                {
                    lock (sr_GetOrCreateContext)
                    {
                        if (s_AppConfig == null)
                        {
                            s_AppConfig = readFromFileOrLoadDeafultSetting();
                        }
                    }
                }

                return s_AppConfig;
            }
        }

        private static AppConfig readFromFileOrLoadDeafultSetting()
        {
            AppConfig appConfigSettings = null;

            if (File.Exists("AppConfig.xml"))
            {
                using (Stream stream = new FileStream("AppConfig.xml", FileMode.OpenOrCreate))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
                    appConfigSettings = (AppConfig) serializer.Deserialize(stream);
                }
            }
            else
            {
                appConfigSettings = new AppConfig()
                {
                    AutoConnect = false
                };
            }

            return appConfigSettings;
        }

        public string AccessToken { get; set; }
        
        public Point WindowLocation { get; set; }

        public bool AutoConnect { get; set; }

        public void SaveToFile()
        {
            using (Stream stream = new FileStream("AppConfig.xml", FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof (AppConfig));
                serializer.Serialize(stream, this);
            }
        }
    }
}
