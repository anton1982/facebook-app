﻿namespace A16_Ex02_Anton_305939050_Daniel_305698912
{
    public partial class FormFacebookMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonLogin = new System.Windows.Forms.Button();
            this.userProfileInfo = new System.Windows.Forms.GroupBox();
            this.labelUsername = new System.Windows.Forms.Label();
            this.labelGender = new System.Windows.Forms.Label();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.userProfilePictureBox = new System.Windows.Forms.PictureBox();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.labelAttachedPhoto = new System.Windows.Forms.Label();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.checkBoxAutoConnect = new System.Windows.Forms.CheckBox();
            this.buttonPictureFilter = new System.Windows.Forms.Button();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.pictureBoxFriends = new System.Windows.Forms.PictureBox();
            this.labelTypeSearchHere = new System.Windows.Forms.Label();
            this.richTextBoxPostOnFacebook = new System.Windows.Forms.RichTextBox();
            this.buttonPostOnFacebook = new System.Windows.Forms.Button();
            this.dateTimePickerForSearchStartDate = new System.Windows.Forms.DateTimePicker();
            this.labelStartDate = new System.Windows.Forms.Label();
            this.labelEndDate = new System.Windows.Forms.Label();
            this.checkBoxIfDateWasChecked = new System.Windows.Forms.CheckBox();
            this.dateTimePickerForSearchEndDate = new System.Windows.Forms.DateTimePicker();
            this.buttonReset = new System.Windows.Forms.Button();
            this.tabCheckIn = new System.Windows.Forms.TabPage();
            this.dataGridViewCheckIn = new System.Windows.Forms.DataGridView();
            this.tabCheckInName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabCheckInMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkinsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabEvents = new System.Windows.Forms.TabPage();
            this.dataGridViewEvents = new System.Windows.Forms.DataGridView();
            this.tabEventsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabEventsStartTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabEventsDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eventsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPosts = new System.Windows.Forms.TabPage();
            this.dataGridViewPost = new System.Windows.Forms.DataGridView();
            this.postBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControlUserInfo = new System.Windows.Forms.TabControl();
            this.tabFriends = new System.Windows.Forms.TabPage();
            this.dataGridViewFriends = new System.Windows.Forms.DataGridView();
            this.tabFriendName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabFriendUpdateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBoxEvents = new System.Windows.Forms.PictureBox();
            this.tabPostMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPostDescroption = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPostUpdateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.userProfileInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userProfilePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFriends)).BeginInit();
            this.tabCheckIn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCheckIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkinsBindingSource)).BeginInit();
            this.tabEvents.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsBindingSource)).BeginInit();
            this.tabPosts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.postBindingSource)).BeginInit();
            this.tabControlUserInfo.SuspendLayout();
            this.tabFriends.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFriends)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEvents)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLogin
            // 
            this.buttonLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLogin.Location = new System.Drawing.Point(8, 10);
            this.buttonLogin.Margin = new System.Windows.Forms.Padding(2);
            this.buttonLogin.Name = "buttonLogin";
            this.buttonLogin.Size = new System.Drawing.Size(84, 29);
            this.buttonLogin.TabIndex = 1;
            this.buttonLogin.Text = "Login";
            this.buttonLogin.UseVisualStyleBackColor = false;
            this.buttonLogin.Click += new System.EventHandler(this.buttonLoginOrLogout_Click);
            // 
            // userProfileInfo
            // 
            this.userProfileInfo.Controls.Add(this.labelUsername);
            this.userProfileInfo.Controls.Add(this.labelGender);
            this.userProfileInfo.Controls.Add(this.labelBirthday);
            this.userProfileInfo.Location = new System.Drawing.Point(10, 271);
            this.userProfileInfo.Margin = new System.Windows.Forms.Padding(2);
            this.userProfileInfo.Name = "userProfileInfo";
            this.userProfileInfo.Padding = new System.Windows.Forms.Padding(2);
            this.userProfileInfo.Size = new System.Drawing.Size(172, 148);
            this.userProfileInfo.TabIndex = 5;
            this.userProfileInfo.TabStop = false;
            this.userProfileInfo.Text = "User profile information";
            this.userProfileInfo.Enter += new System.EventHandler(this.userProfileInfo_Enter);
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Location = new System.Drawing.Point(4, 36);
            this.labelUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(53, 17);
            this.labelUsername.TabIndex = 22;
            this.labelUsername.Text = "Name :";
            // 
            // labelGender
            // 
            this.labelGender.AutoSize = true;
            this.labelGender.Location = new System.Drawing.Point(4, 110);
            this.labelGender.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelGender.Name = "labelGender";
            this.labelGender.Size = new System.Drawing.Size(64, 17);
            this.labelGender.TabIndex = 25;
            this.labelGender.Text = "Gender :";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(4, 73);
            this.labelBirthday.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(68, 17);
            this.labelBirthday.TabIndex = 23;
            this.labelBirthday.Text = "Birthday :";
            // 
            // userProfilePictureBox
            // 
            this.userProfilePictureBox.ErrorImage = global::A16_Ex02_Anton_305939050_Daniel_305698912.Properties.Resources.FacebookProfilePhoto1;
            this.userProfilePictureBox.Image = global::A16_Ex02_Anton_305939050_Daniel_305698912.Properties.Resources.FacebookProfilePhoto1;
            this.userProfilePictureBox.Location = new System.Drawing.Point(8, 66);
            this.userProfilePictureBox.Margin = new System.Windows.Forms.Padding(2);
            this.userProfilePictureBox.Name = "userProfilePictureBox";
            this.userProfilePictureBox.Size = new System.Drawing.Size(172, 191);
            this.userProfilePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.userProfilePictureBox.TabIndex = 4;
            this.userProfilePictureBox.TabStop = false;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(FacebookWrapper.ObjectModel.User);
            // 
            // labelAttachedPhoto
            // 
            this.labelAttachedPhoto.AutoSize = true;
            this.labelAttachedPhoto.Location = new System.Drawing.Point(184, 256);
            this.labelAttachedPhoto.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelAttachedPhoto.Name = "labelAttachedPhoto";
            this.labelAttachedPhoto.Size = new System.Drawing.Size(109, 17);
            this.labelAttachedPhoto.TabIndex = 15;
            this.labelAttachedPhoto.Text = "Attached Photo:";
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.Location = new System.Drawing.Point(368, 32);
            this.textBoxSearch.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSearch.Multiline = true;
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(427, 29);
            this.textBoxSearch.TabIndex = 17;
            this.textBoxSearch.TextChanged += new System.EventHandler(this.textBoxSearchOrPost_TextChanged);
            // 
            // checkBoxAutoConnect
            // 
            this.checkBoxAutoConnect.AutoSize = true;
            this.checkBoxAutoConnect.Location = new System.Drawing.Point(8, 44);
            this.checkBoxAutoConnect.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxAutoConnect.Name = "checkBoxAutoConnect";
            this.checkBoxAutoConnect.Size = new System.Drawing.Size(115, 21);
            this.checkBoxAutoConnect.TabIndex = 22;
            this.checkBoxAutoConnect.Text = "Auto Connect";
            this.checkBoxAutoConnect.UseVisualStyleBackColor = true;
            this.checkBoxAutoConnect.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // buttonPictureFilter
            // 
            this.buttonPictureFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonPictureFilter.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPictureFilter.Location = new System.Drawing.Point(184, 218);
            this.buttonPictureFilter.Margin = new System.Windows.Forms.Padding(2);
            this.buttonPictureFilter.Name = "buttonPictureFilter";
            this.buttonPictureFilter.Size = new System.Drawing.Size(173, 36);
            this.buttonPictureFilter.TabIndex = 23;
            this.buttonPictureFilter.Text = "Filter a Picture";
            this.buttonPictureFilter.UseVisualStyleBackColor = false;
            this.buttonPictureFilter.Click += new System.EventHandler(this.buttonPictureFilter_Click);
            // 
            // buttonConnect
            // 
            this.buttonConnect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonConnect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonConnect.Location = new System.Drawing.Point(98, 10);
            this.buttonConnect.Margin = new System.Windows.Forms.Padding(2);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(85, 29);
            this.buttonConnect.TabIndex = 24;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = false;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // pictureBoxFriends
            // 
            this.pictureBoxFriends.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.userBindingSource, "ImageLarge", true));
            this.pictureBoxFriends.ErrorImage = null;
            this.pictureBoxFriends.Location = new System.Drawing.Point(183, 275);
            this.pictureBoxFriends.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxFriends.Name = "pictureBoxFriends";
            this.pictureBoxFriends.Size = new System.Drawing.Size(174, 144);
            this.pictureBoxFriends.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxFriends.TabIndex = 26;
            this.pictureBoxFriends.TabStop = false;
            // 
            // labelTypeSearchHere
            // 
            this.labelTypeSearchHere.AutoSize = true;
            this.labelTypeSearchHere.Location = new System.Drawing.Point(369, 9);
            this.labelTypeSearchHere.Name = "labelTypeSearchHere";
            this.labelTypeSearchHere.Size = new System.Drawing.Size(354, 17);
            this.labelTypeSearchHere.TabIndex = 28;
            this.labelTypeSearchHere.Text = "Search here for your posts, events, checkins or friends";
            // 
            // richTextBoxPostOnFacebook
            // 
            this.richTextBoxPostOnFacebook.Location = new System.Drawing.Point(185, 66);
            this.richTextBoxPostOnFacebook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBoxPostOnFacebook.Name = "richTextBoxPostOnFacebook";
            this.richTextBoxPostOnFacebook.Size = new System.Drawing.Size(174, 48);
            this.richTextBoxPostOnFacebook.TabIndex = 30;
            this.richTextBoxPostOnFacebook.Tag = "";
            this.richTextBoxPostOnFacebook.Text = "Please Enter Your Post (Optional)";
            this.richTextBoxPostOnFacebook.MouseClick += new System.Windows.Forms.MouseEventHandler(this.richTextBoxPostOnFacebook_MouseDown);
            // 
            // buttonPostOnFacebook
            // 
            this.buttonPostOnFacebook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonPostOnFacebook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPostOnFacebook.Location = new System.Drawing.Point(185, 120);
            this.buttonPostOnFacebook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonPostOnFacebook.Name = "buttonPostOnFacebook";
            this.buttonPostOnFacebook.Size = new System.Drawing.Size(173, 37);
            this.buttonPostOnFacebook.TabIndex = 29;
            this.buttonPostOnFacebook.Text = "Post On Facebook";
            this.buttonPostOnFacebook.UseVisualStyleBackColor = false;
            this.buttonPostOnFacebook.Click += new System.EventHandler(this.buttonPostOnFacebook_Click);
            // 
            // dateTimePickerForSearchStartDate
            // 
            this.dateTimePickerForSearchStartDate.Checked = false;
            this.dateTimePickerForSearchStartDate.Enabled = false;
            this.dateTimePickerForSearchStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerForSearchStartDate.Location = new System.Drawing.Point(504, 86);
            this.dateTimePickerForSearchStartDate.Name = "dateTimePickerForSearchStartDate";
            this.dateTimePickerForSearchStartDate.Size = new System.Drawing.Size(99, 23);
            this.dateTimePickerForSearchStartDate.TabIndex = 31;
            this.dateTimePickerForSearchStartDate.ValueChanged += new System.EventHandler(this.dateTimePickerForSearchStartDate_ValueChanged);
            // 
            // labelStartDate
            // 
            this.labelStartDate.AutoSize = true;
            this.labelStartDate.Location = new System.Drawing.Point(501, 66);
            this.labelStartDate.Name = "labelStartDate";
            this.labelStartDate.Size = new System.Drawing.Size(72, 17);
            this.labelStartDate.TabIndex = 33;
            this.labelStartDate.Text = "Start Date";
            // 
            // labelEndDate
            // 
            this.labelEndDate.AutoSize = true;
            this.labelEndDate.Location = new System.Drawing.Point(606, 65);
            this.labelEndDate.Name = "labelEndDate";
            this.labelEndDate.Size = new System.Drawing.Size(67, 17);
            this.labelEndDate.TabIndex = 34;
            this.labelEndDate.Text = "End Date";
            // 
            // checkBoxIfDateWasChecked
            // 
            this.checkBoxIfDateWasChecked.AutoSize = true;
            this.checkBoxIfDateWasChecked.Location = new System.Drawing.Point(372, 86);
            this.checkBoxIfDateWasChecked.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxIfDateWasChecked.Name = "checkBoxIfDateWasChecked";
            this.checkBoxIfDateWasChecked.Size = new System.Drawing.Size(127, 21);
            this.checkBoxIfDateWasChecked.TabIndex = 35;
            this.checkBoxIfDateWasChecked.Text = "Date (Optional)";
            this.checkBoxIfDateWasChecked.UseVisualStyleBackColor = true;
            this.checkBoxIfDateWasChecked.CheckedChanged += new System.EventHandler(this.checkBoxIfDateWasChecked_CheckedChanged);
            // 
            // dateTimePickerForSearchEndDate
            // 
            this.dateTimePickerForSearchEndDate.Checked = false;
            this.dateTimePickerForSearchEndDate.Enabled = false;
            this.dateTimePickerForSearchEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerForSearchEndDate.Location = new System.Drawing.Point(609, 86);
            this.dateTimePickerForSearchEndDate.Name = "dateTimePickerForSearchEndDate";
            this.dateTimePickerForSearchEndDate.Size = new System.Drawing.Size(99, 23);
            this.dateTimePickerForSearchEndDate.TabIndex = 32;
            this.dateTimePickerForSearchEndDate.ValueChanged += new System.EventHandler(this.dateTimePickerForSearchEndDate_ValueChanged);
            // 
            // buttonReset
            // 
            this.buttonReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonReset.Location = new System.Drawing.Point(727, 86);
            this.buttonReset.Margin = new System.Windows.Forms.Padding(2);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(68, 23);
            this.buttonReset.TabIndex = 36;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // tabCheckIn
            // 
            this.tabCheckIn.AutoScroll = true;
            this.tabCheckIn.Controls.Add(this.dataGridViewCheckIn);
            this.tabCheckIn.Location = new System.Drawing.Point(4, 26);
            this.tabCheckIn.Name = "tabCheckIn";
            this.tabCheckIn.Size = new System.Drawing.Size(423, 269);
            this.tabCheckIn.TabIndex = 2;
            this.tabCheckIn.Text = "CheckIn";
            this.tabCheckIn.UseVisualStyleBackColor = true;
            // 
            // dataGridViewCheckIn
            // 
            this.dataGridViewCheckIn.AllowUserToAddRows = false;
            this.dataGridViewCheckIn.AllowUserToDeleteRows = false;
            this.dataGridViewCheckIn.AutoGenerateColumns = false;
            this.dataGridViewCheckIn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCheckIn.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tabCheckInName,
            this.tabCheckInMessage,
            this.idDataGridViewTextBoxColumn});
            this.dataGridViewCheckIn.DataSource = this.checkinsBindingSource;
            this.dataGridViewCheckIn.Location = new System.Drawing.Point(-4, 0);
            this.dataGridViewCheckIn.Name = "dataGridViewCheckIn";
            this.dataGridViewCheckIn.ReadOnly = true;
            this.dataGridViewCheckIn.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dataGridViewCheckIn.Size = new System.Drawing.Size(421, 270);
            this.dataGridViewCheckIn.TabIndex = 0;
            this.dataGridViewCheckIn.SelectionChanged += new System.EventHandler(this.dataGridViewCheckIn_SelectionChanged);
            // 
            // tabCheckInName
            // 
            this.tabCheckInName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabCheckInName.DataPropertyName = "Name";
            this.tabCheckInName.HeaderText = "Name";
            this.tabCheckInName.Name = "tabCheckInName";
            this.tabCheckInName.ReadOnly = true;
            // 
            // tabCheckInMessage
            // 
            this.tabCheckInMessage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabCheckInMessage.DataPropertyName = "Message";
            this.tabCheckInMessage.HeaderText = "Message";
            this.tabCheckInMessage.Name = "tabCheckInMessage";
            this.tabCheckInMessage.ReadOnly = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Count Of Likes";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // checkinsBindingSource
            // 
            this.checkinsBindingSource.DataMember = "Checkins";
            this.checkinsBindingSource.DataSource = this.userBindingSource;
            // 
            // tabEvents
            // 
            this.tabEvents.AutoScroll = true;
            this.tabEvents.Controls.Add(this.dataGridViewEvents);
            this.tabEvents.Location = new System.Drawing.Point(4, 26);
            this.tabEvents.Name = "tabEvents";
            this.tabEvents.Padding = new System.Windows.Forms.Padding(3);
            this.tabEvents.Size = new System.Drawing.Size(423, 269);
            this.tabEvents.TabIndex = 1;
            this.tabEvents.Text = "Events";
            this.tabEvents.UseVisualStyleBackColor = true;
            // 
            // dataGridViewEvents
            // 
            this.dataGridViewEvents.AllowUserToAddRows = false;
            this.dataGridViewEvents.AllowUserToDeleteRows = false;
            this.dataGridViewEvents.AutoGenerateColumns = false;
            this.dataGridViewEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEvents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tabEventsName,
            this.tabEventsStartTime,
            this.tabEventsDescription});
            this.dataGridViewEvents.DataSource = this.eventsBindingSource;
            this.dataGridViewEvents.Location = new System.Drawing.Point(-4, 0);
            this.dataGridViewEvents.MultiSelect = false;
            this.dataGridViewEvents.Name = "dataGridViewEvents";
            this.dataGridViewEvents.ReadOnly = true;
            this.dataGridViewEvents.Size = new System.Drawing.Size(421, 270);
            this.dataGridViewEvents.TabIndex = 0;
            this.dataGridViewEvents.SelectionChanged += new System.EventHandler(this.dataGridViewEvents_SelectionChanged);
            // 
            // tabEventsName
            // 
            this.tabEventsName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabEventsName.DataPropertyName = "Name";
            this.tabEventsName.HeaderText = "Name";
            this.tabEventsName.Name = "tabEventsName";
            this.tabEventsName.ReadOnly = true;
            // 
            // tabEventsStartTime
            // 
            this.tabEventsStartTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabEventsStartTime.DataPropertyName = "StartTime";
            this.tabEventsStartTime.HeaderText = "StartTime";
            this.tabEventsStartTime.Name = "tabEventsStartTime";
            this.tabEventsStartTime.ReadOnly = true;
            // 
            // tabEventsDescription
            // 
            this.tabEventsDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabEventsDescription.DataPropertyName = "Description";
            this.tabEventsDescription.HeaderText = "Description";
            this.tabEventsDescription.Name = "tabEventsDescription";
            this.tabEventsDescription.ReadOnly = true;
            // 
            // eventsBindingSource
            // 
            this.eventsBindingSource.DataMember = "Events";
            this.eventsBindingSource.DataSource = this.userBindingSource;
            // 
            // tabPosts
            // 
            this.tabPosts.AutoScroll = true;
            this.tabPosts.Controls.Add(this.dataGridViewPost);
            this.tabPosts.Location = new System.Drawing.Point(4, 26);
            this.tabPosts.Name = "tabPosts";
            this.tabPosts.Padding = new System.Windows.Forms.Padding(3);
            this.tabPosts.Size = new System.Drawing.Size(423, 269);
            this.tabPosts.TabIndex = 0;
            this.tabPosts.Text = "Posts";
            this.tabPosts.UseVisualStyleBackColor = true;
            // 
            // dataGridViewPost
            // 
            this.dataGridViewPost.AllowUserToAddRows = false;
            this.dataGridViewPost.AllowUserToDeleteRows = false;
            this.dataGridViewPost.AutoGenerateColumns = false;
            this.dataGridViewPost.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPost.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tabPostMessage,
            this.tabPostDescroption,
            this.tabPostUpdateTime});
            this.dataGridViewPost.DataSource = this.postBindingSource;
            this.dataGridViewPost.Location = new System.Drawing.Point(-4, 0);
            this.dataGridViewPost.MultiSelect = false;
            this.dataGridViewPost.Name = "dataGridViewPost";
            this.dataGridViewPost.ReadOnly = true;
            this.dataGridViewPost.Size = new System.Drawing.Size(421, 270);
            this.dataGridViewPost.TabIndex = 0;
            this.dataGridViewPost.SelectionChanged += new System.EventHandler(this.dataGridViewPost_SelectionChanged);
            // 
            // postBindingSource
            // 
            this.postBindingSource.DataSource = typeof(FacebookWrapper.ObjectModel.Post);
            // 
            // tabControlUserInfo
            // 
            this.tabControlUserInfo.Controls.Add(this.tabPosts);
            this.tabControlUserInfo.Controls.Add(this.tabEvents);
            this.tabControlUserInfo.Controls.Add(this.tabCheckIn);
            this.tabControlUserInfo.Controls.Add(this.tabFriends);
            this.tabControlUserInfo.Location = new System.Drawing.Point(368, 120);
            this.tabControlUserInfo.Name = "tabControlUserInfo";
            this.tabControlUserInfo.SelectedIndex = 0;
            this.tabControlUserInfo.Size = new System.Drawing.Size(431, 299);
            this.tabControlUserInfo.TabIndex = 25;
            this.tabControlUserInfo.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControlUserInfo_Selected_1);
            // 
            // tabFriends
            // 
            this.tabFriends.AutoScroll = true;
            this.tabFriends.Controls.Add(this.dataGridViewFriends);
            this.tabFriends.Location = new System.Drawing.Point(4, 26);
            this.tabFriends.Name = "tabFriends";
            this.tabFriends.Padding = new System.Windows.Forms.Padding(3);
            this.tabFriends.Size = new System.Drawing.Size(423, 269);
            this.tabFriends.TabIndex = 3;
            this.tabFriends.Text = "Friends";
            this.tabFriends.UseVisualStyleBackColor = true;
            // 
            // dataGridViewFriends
            // 
            this.dataGridViewFriends.AllowUserToAddRows = false;
            this.dataGridViewFriends.AllowUserToDeleteRows = false;
            this.dataGridViewFriends.AutoGenerateColumns = false;
            this.dataGridViewFriends.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFriends.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tabFriendName,
            this.tabFriendUpdateTime});
            this.dataGridViewFriends.DataSource = this.userBindingSource;
            this.dataGridViewFriends.Location = new System.Drawing.Point(-4, 0);
            this.dataGridViewFriends.MultiSelect = false;
            this.dataGridViewFriends.Name = "dataGridViewFriends";
            this.dataGridViewFriends.ReadOnly = true;
            this.dataGridViewFriends.Size = new System.Drawing.Size(421, 270);
            this.dataGridViewFriends.TabIndex = 0;
            this.dataGridViewFriends.SelectionChanged += new System.EventHandler(this.dataGridViewFriends_SelectionChanged);
            // 
            // tabFriendName
            // 
            this.tabFriendName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabFriendName.DataPropertyName = "Name";
            this.tabFriendName.HeaderText = "Name";
            this.tabFriendName.Name = "tabFriendName";
            this.tabFriendName.ReadOnly = true;
            // 
            // tabFriendUpdateTime
            // 
            this.tabFriendUpdateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabFriendUpdateTime.DataPropertyName = "UpdateTime";
            this.tabFriendUpdateTime.HeaderText = "UpdateTime";
            this.tabFriendUpdateTime.Name = "tabFriendUpdateTime";
            this.tabFriendUpdateTime.ReadOnly = true;
            // 
            // pictureBoxEvents
            // 
            this.pictureBoxEvents.DataBindings.Add(new System.Windows.Forms.Binding("Image", this.eventsBindingSource, "ImageLarge", true));
            this.pictureBoxEvents.ErrorImage = null;
            this.pictureBoxEvents.Location = new System.Drawing.Point(183, 275);
            this.pictureBoxEvents.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBoxEvents.Name = "pictureBoxEvents";
            this.pictureBoxEvents.Size = new System.Drawing.Size(174, 144);
            this.pictureBoxEvents.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxEvents.TabIndex = 27;
            this.pictureBoxEvents.TabStop = false;
            // 
            // tabPostMessage
            // 
            this.tabPostMessage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabPostMessage.DataPropertyName = "Message";
            this.tabPostMessage.HeaderText = "Message";
            this.tabPostMessage.Name = "tabPostMessage";
            this.tabPostMessage.ReadOnly = true;
            // 
            // tabPostDescroption
            // 
            this.tabPostDescroption.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabPostDescroption.DataPropertyName = "Description";
            this.tabPostDescroption.HeaderText = "Description";
            this.tabPostDescroption.Name = "tabPostDescroption";
            this.tabPostDescroption.ReadOnly = true;
            // 
            // tabPostUpdateTime
            // 
            this.tabPostUpdateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tabPostUpdateTime.DataPropertyName = "UpdateTime";
            this.tabPostUpdateTime.HeaderText = "UpdateTime";
            this.tabPostUpdateTime.Name = "tabPostUpdateTime";
            this.tabPostUpdateTime.ReadOnly = true;
            // 
            // FormFacebookMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(208)))), ((int)(((byte)(225)))));
            this.ClientSize = new System.Drawing.Size(803, 422);
            this.Controls.Add(this.pictureBoxEvents);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.checkBoxIfDateWasChecked);
            this.Controls.Add(this.labelEndDate);
            this.Controls.Add(this.labelStartDate);
            this.Controls.Add(this.dateTimePickerForSearchEndDate);
            this.Controls.Add(this.dateTimePickerForSearchStartDate);
            this.Controls.Add(this.richTextBoxPostOnFacebook);
            this.Controls.Add(this.buttonPostOnFacebook);
            this.Controls.Add(this.labelTypeSearchHere);
            this.Controls.Add(this.tabControlUserInfo);
            this.Controls.Add(this.pictureBoxFriends);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.buttonPictureFilter);
            this.Controls.Add(this.checkBoxAutoConnect);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.labelAttachedPhoto);
            this.Controls.Add(this.userProfileInfo);
            this.Controls.Add(this.userProfilePictureBox);
            this.Controls.Add(this.buttonLogin);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormFacebookMain";
            this.Text = "FaceApp - Anton & Daniel";
            this.Load += new System.EventHandler(this.formFacebookMain_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.FormFacebookMain_MouseClick);
            this.userProfileInfo.ResumeLayout(false);
            this.userProfileInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userProfilePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFriends)).EndInit();
            this.tabCheckIn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCheckIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkinsBindingSource)).EndInit();
            this.tabEvents.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventsBindingSource)).EndInit();
            this.tabPosts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.postBindingSource)).EndInit();
            this.tabControlUserInfo.ResumeLayout(false);
            this.tabFriends.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFriends)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxEvents)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLogin;
        private System.Windows.Forms.PictureBox userProfilePictureBox;
        private System.Windows.Forms.GroupBox userProfileInfo;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.Label labelGender;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelAttachedPhoto;
        private System.Windows.Forms.CheckBox checkBoxAutoConnect;
        private System.Windows.Forms.Button buttonPictureFilter;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.PictureBox pictureBoxFriends;
        private System.Windows.Forms.Label labelTypeSearchHere;
        private System.Windows.Forms.RichTextBox richTextBoxPostOnFacebook;
        private System.Windows.Forms.Button buttonPostOnFacebook;
        private System.Windows.Forms.Label labelStartDate;
        private System.Windows.Forms.Label labelEndDate;
        private System.Windows.Forms.CheckBox checkBoxIfDateWasChecked;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.TabControl tabControlUserInfo;
        private System.Windows.Forms.DataGridView dataGridViewFriends;
        private System.Windows.Forms.DataGridView dataGridViewEvents;
        private System.Windows.Forms.DataGridView dataGridViewCheckIn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabCheckInName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabCheckInMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabEventsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabEventsStartTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabEventsDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabFriendName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabFriendUpdateTime;
        private System.Windows.Forms.PictureBox pictureBoxEvents;
        internal System.Windows.Forms.TextBox textBoxSearch;
        internal System.Windows.Forms.DateTimePicker dateTimePickerForSearchStartDate;
        internal System.Windows.Forms.DateTimePicker dateTimePickerForSearchEndDate;
        internal System.Windows.Forms.TabPage tabPosts;
        internal System.Windows.Forms.DataGridView dataGridViewPost;
        internal System.Windows.Forms.BindingSource postBindingSource;
        internal System.Windows.Forms.TabPage tabCheckIn;
        internal System.Windows.Forms.TabPage tabEvents;
        internal System.Windows.Forms.TabPage tabFriends;
        internal System.Windows.Forms.BindingSource userBindingSource;
        internal System.Windows.Forms.BindingSource eventsBindingSource;
        internal System.Windows.Forms.BindingSource checkinsBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabPostMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabPostDescroption;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabPostUpdateTime;
    }
}
