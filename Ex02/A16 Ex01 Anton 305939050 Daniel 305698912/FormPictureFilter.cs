﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;

namespace A16_Ex02_Anton_305939050_Daniel_305698912
{
    public partial class FormPictureFilter : Form
    {
        private readonly PictureFilterLogic r_PictureFilterLogic;
        private readonly int r_TimerForChangeWindowFromSize = 250;
        private readonly bool r_IsCheckedOrEnabled = true;
        private string m_PostMessage;

        public FormPictureFilter()
        {
            InitializeComponent();
            formInitializer();
            r_PictureFilterLogic = new PictureFilterLogic();
        }

        private PictureFilterLogic pictureFilterLogic
        {
            get
            {
                return r_PictureFilterLogic;
            }
        }      
        
        private bool isCheckedOrEnabled
        {
            get { return r_IsCheckedOrEnabled; }
        }

        private int timerForChangeWindowFromSize
        {
            get { return r_TimerForChangeWindowFromSize; }
        }

        public string PostMessage
        {
            get { return m_PostMessage; }
            set { m_PostMessage = value; }
        }

        private void formInitializer()
        {
            setRadioButtonsEnableCheckState(!isCheckedOrEnabled);
            setButtonsPressedState(isCheckedOrEnabled);
            setRichTextBoxPostOnFacebookToDefaultText();
            pictureBoxImageRemover();
        }

        private void setButtonsPressedState(bool i_IsPressed)
        {
            setButtonLoadFromFileEnableState(i_IsPressed);
            setbuttonLoadProfileFromFacebookEnableState(i_IsPressed);
            setButtonGoEnableState(!i_IsPressed);
            setRichTextBoxPostOnFacebookEnableState(!i_IsPressed);
            setButtonPostOnFacebookEnableState(!i_IsPressed);
            setButtonChooseYourPictureAgainEnableState(!i_IsPressed);
            setButtonResetEnableState(!i_IsPressed);
            setButtonSaveFileEnableState(!i_IsPressed);
        }

        private void setRadioButtonsEnableCheckState(bool i_IsCheckedEnable)
        {
            radioButtonGrays.Checked = i_IsCheckedEnable;
            radioButtonGrays.Enabled = i_IsCheckedEnable;
            radioButtonNegative.Enabled = i_IsCheckedEnable;
            radioButtonMirror.Enabled = i_IsCheckedEnable;
            radioButtonRotateLeft.Enabled = i_IsCheckedEnable;
            radioButtonRotateRight.Enabled = i_IsCheckedEnable;
            radioButtonRandom.Enabled = i_IsCheckedEnable;
        }

        private void setButtonGoEnableState(bool i_IsEnabled)
        {
            buttonGo.Enabled = i_IsEnabled;
        }

        private void setButtonPostOnFacebookEnableState(bool i_IsEnabled)
        {
            buttonPostOnFacebook.Enabled = i_IsEnabled;
        }

        private void setButtonChooseYourPictureAgainEnableState(bool i_IsEnabled)
        {
            buttonChooseYourPictureAgain.Enabled = i_IsEnabled;
        }

        private void setButtonLoadFromFileEnableState(bool i_IsEnabled)
        {
            buttonLoadFromFile.Enabled = i_IsEnabled;
        }

        private void setbuttonLoadProfileFromFacebookEnableState(bool i_IsEnabled)
        {
            buttonLoadProfileFromFacebook.Enabled = i_IsEnabled;
        }

        private void setButtonSaveFileEnableState(bool i_IsEnabled)
        {
            buttonSaveFile.Enabled = i_IsEnabled;
        }

        private void setButtonResetEnableState(bool i_IsEnabled)
        {
            buttonReset.Enabled = i_IsEnabled;
        }

        private void setRichTextBoxPostOnFacebookEnableState(bool i_IsEnabled)
        {
            richTextBoxPostOnFacebook.Enabled = i_IsEnabled;
        }

        private void setRichTextBoxPostOnFacebookToDefaultText()
        {
            richTextBoxPostOnFacebook.Text = @"Please Enter Your Post (Optional)";
        }

        private void setProportyAfterLoadButtonsPressed()
        {
            setButtonsPressedState(!isCheckedOrEnabled);
            setRadioButtonsEnableCheckState(isCheckedOrEnabled);
            assignOriginalPictureInPictureBox();
            changeWindowsFormSize();
        }

        private void pictureBoxImageRemover()
        {
            pictureBoxLoadedPicture.Image = null;
            pictureBoxAfterPicture.Image = null;
        }

        private void assignOriginalPictureInPictureBox()
        {
            try
            {
                pictureBoxImageRemover();

                pictureBoxLoadedPicture.Load(pictureFilterLogic.OriginalPicturePath);
                pictureBoxAfterPicture.Load(pictureFilterLogic.OriginalPicturePath);

                if (pictureFilterLogic.OriginalBitmap == null || pictureFilterLogic.CopyOfOriginalBitmap == null)
                {
                    pictureFilterLogic.OriginalBitmap = new Bitmap(pictureBoxLoadedPicture.Image);
                    pictureFilterLogic.CopyOfOriginalBitmap = new Bitmap(pictureBoxLoadedPicture.Image);
                }
                else
                {
                    pictureFilterLogic.OriginalBitmap = pictureBoxLoadedPicture.Image as Bitmap;
                    pictureFilterLogic.CopyOfOriginalBitmap = pictureBoxLoadedPicture.Image.Clone() as Bitmap;
                }
            }
            catch (Exception ex)
            {   
                throw new Exception(ex.Message);
            }
        }

        private void assignFilteredPictureInPictureBox(Bitmap i_PictureInPictureBox)
        {
            pictureBoxLoadedPicture.Refresh();
            pictureBoxAfterPicture.Image = i_PictureInPictureBox;
        }

        private void changeWindowsFormSize()
        {
            for (int i = 0; i < timerForChangeWindowFromSize; i++)
            {
                Width = Width + 2;
                Height++;
            }
        }

        private void changeWindowFormSizeToDefault()
        {
            for (int i = 0; i < timerForChangeWindowFromSize; i++)
            {
                Width = Width - 2;
                Height--;
            }
        }

        private void radioButtonGrays_CheckedChanged(object sender, EventArgs e)
        {
            pictureFilterLogic.FilterOptionsType = PictureFilterLogic.eFilterOptions.FilterGray;
        }

        private void radioButtonMirror_CheckedChanged(object sender, EventArgs e)
        {
            pictureFilterLogic.FilterOptionsType = PictureFilterLogic.eFilterOptions.FilterMirror;
        }

        private void radioButtonNegative_CheckedChanged(object sender, EventArgs e)
        {
            pictureFilterLogic.FilterOptionsType = PictureFilterLogic.eFilterOptions.FilterNegative;
        }

        private void radioButtonRotateLeft_CheckedChanged(object sender, EventArgs e)
        {
            pictureFilterLogic.FilterOptionsType = PictureFilterLogic.eFilterOptions.FilterRotateLeft;
        }

        private void radioButtonRotateRight_CheckedChanged(object sender, EventArgs e)
        {
            pictureFilterLogic.FilterOptionsType = PictureFilterLogic.eFilterOptions.FilterRotateRight;
        }

        private void radioButtonRandom_CheckedChanged(object sender, EventArgs e)
        {
            pictureFilterLogic.FilterOptionsType = PictureFilterLogic.eFilterOptions.RandomBetweenFilters;
        }
        
        private void buttonPostOnFacebook_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileHiddenFromUser("bitmapAfterEdit.jpg");
                savedFileURL("bitmapAfterEdit.jpg");
                MessaegeFromUserToPost();
                
                LoggedInUser.Instance.CurrentUser.PostPhoto(pictureFilterLogic.PicturePathAfterEdit, PostMessage);

                messageToUser("Your Post Is On Facebook Now.");
            }
            catch (Exception)
            {
                messageToUser("Your Post Is Not On Facebook.");
            }
        }

        private void savedFileURL(string i_FileName)
        {
            pictureFilterLogic.SetSavedFilePath(AppDomain.CurrentDomain.BaseDirectory + "\\" + i_FileName);
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            assignOriginalPictureInPictureBox();
            setRichTextBoxPostOnFacebookToDefaultText();
            setRadioButtonsEnableCheckState(isCheckedOrEnabled);
        }

        private void buttonChooseYourPictureAgain_Click(object sender, EventArgs e)
        {
            formInitializer();
            changeWindowFormSizeToDefault();
        }

        private void buttonSaveFile_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog.Filter = pictureFilterLogic.FilterChooseBetweenPictures;
                saveFileDialog.FileName = "Enter File Name";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    pictureBoxAfterPicture.Image.Save(saveFileDialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    messageToUser("Your picture was successfully saved.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show(@"Your picture was not saved.");
            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            switch (pictureFilterLogic.FilterOptionsType)
            {
                case PictureFilterLogic.eFilterOptions.FilterGray:
                    {
                        pictureFilterLogic.ChangePictureToGrays();
                        assignFilteredPictureInPictureBox(pictureFilterLogic.CopyOfOriginalBitmap);
                        break;
                    }

                case PictureFilterLogic.eFilterOptions.FilterNegative:
                    {
                        pictureFilterLogic.ChangePictureToNegative();
                        assignFilteredPictureInPictureBox(pictureFilterLogic.CopyOfOriginalBitmap);
                        break;
                    }

                case PictureFilterLogic.eFilterOptions.FilterMirror:
                    {
                        pictureFilterLogic.ChangePictureToMirrorPicture();
                        assignFilteredPictureInPictureBox(pictureFilterLogic.CopyOfOriginalBitmap);
                        break;
                    }

                case PictureFilterLogic.eFilterOptions.FilterRotateLeft:
                    {
                        pictureFilterLogic.RotatePictureLeft();
                        assignFilteredPictureInPictureBox(pictureFilterLogic.CopyOfOriginalBitmap);
                        break;
                    }

                case PictureFilterLogic.eFilterOptions.FilterRotateRight:
                    {
                        pictureFilterLogic.RotatePictureRight();
                        assignFilteredPictureInPictureBox(pictureFilterLogic.CopyOfOriginalBitmap);
                        break;
                    }

                case PictureFilterLogic.eFilterOptions.RandomBetweenFilters:
                    {
                        pictureFilterLogic.RandomPictureFilter();
                        buttonGo.PerformClick();
                        pictureFilterLogic.FilterOptionsType = PictureFilterLogic.eFilterOptions.RandomBetweenFilters;
                        break;
                    }

                default:
                    {
                        MessageBox.Show(@"Please choose an option.");
                        break;
                    }
            }
        }

        private void buttonLoadFromFile_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog.Filter = pictureFilterLogic.FilterChooseBetweenPictures;
                openFileDialog.FileName = "Select File";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    pictureFilterLogic.OriginalPicturePath = openFileDialog.FileName;
                    setProportyAfterLoadButtonsPressed();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(@"Picture was not choosen.");
            }
        }

        private void buttonLoadFromFacebook_Click(object sender, EventArgs e)
        {
            try
            {
                pictureFilterLogic.OriginalPicturePath = LoggedInUser.Instance.CurrentUser.PictureLargeURL;

                setProportyAfterLoadButtonsPressed();
            }
            catch (Exception ex)
            {
                messageToUser(ex.Message);
            }
        }

        private void richTextBoxPostOnFacebook_MouseDown(object sender, EventArgs e)
        {
            richTextBoxPostOnFacebook.Text = string.Empty;
        }

        private void saveFileHiddenFromUser(string i_FileName)
        {
            try
            {
                pictureBoxAfterPicture.Image.Save(AppDomain.CurrentDomain.BaseDirectory + "\\" + i_FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (Exception ex)
            {
                throw new Exception("File was not saved!");
            }
        }

        private void MessaegeFromUserToPost()
        {
            string defaultPost = "Please Enter Your Post (Optional)";
            PostMessage = string.Empty;

            if (!richTextBoxPostOnFacebook.Equals(defaultPost))
            {
                PostMessage = richTextBoxPostOnFacebook.Text;
            }
        }

        private void messageToUser(string i_MsgToUser)
        {
            MessageBox.Show(i_MsgToUser);
        }
    }
}