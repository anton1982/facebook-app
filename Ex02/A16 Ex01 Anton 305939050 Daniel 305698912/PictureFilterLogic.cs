﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using FacebookWrapper.ObjectModel;

namespace A16_Ex02_Anton_305939050_Daniel_305698912
{
    public class PictureFilterLogic
    {
        public enum eFilterOptions
        {
            FilterGray = 1,
            FilterNegative,
            FilterMirror,
            FilterRotateLeft,
            FilterRotateRight,
            RandomBetweenFilters
        }

        private readonly string r_FilterChooseBetweenPictures = "Jpeg Files (.jpg)|*.jpg|Bitmap Files (.bmp)|*.bmp|Gif Files (.gif)|*.gif|PNG Files (.png)|*.png";
        private readonly Random r_RandomNumberBetweenFiltering;
        private eFilterOptions m_FilterOptionsType;
        private string m_OriginalPicturePath;
        private string m_PicturePathAfterEdit;
        private Bitmap m_OriginalBitmap, m_CopyOfOriginalBitmap;

        public PictureFilterLogic()
        {
            r_RandomNumberBetweenFiltering = new Random();
        }

        public eFilterOptions FilterOptionsType
        {
            get { return m_FilterOptionsType; }
            set { m_FilterOptionsType = value; }
        }

        public string OriginalPicturePath
        {
            get { return m_OriginalPicturePath; }
            set { m_OriginalPicturePath = value; }
        }

        public string PicturePathAfterEdit
        {
            get { return m_PicturePathAfterEdit; }
            private set { m_PicturePathAfterEdit = value; }
        }

        private Random randomNumberBetweenFiltering
        {
            get { return r_RandomNumberBetweenFiltering; }
        }

        public Bitmap OriginalBitmap
        {
            get { return m_OriginalBitmap; }
            set { m_OriginalBitmap = value; }
        }

        public Bitmap CopyOfOriginalBitmap
        {
            get { return m_CopyOfOriginalBitmap; }
            set { m_CopyOfOriginalBitmap = value; }
        }

        public string FilterChooseBetweenPictures
        {
            get { return r_FilterChooseBetweenPictures; }
        }

        public void ChangePictureToGrays()
        {
            for (int i = 0; i < CopyOfOriginalBitmap.Height; i++)
            {
                for (int j = 0; j < CopyOfOriginalBitmap.Width; j++)
                {
                    int gray = (int)((CopyOfOriginalBitmap.GetPixel(j, i).R * 0.3) + (CopyOfOriginalBitmap.GetPixel(j, i).G * 0.59) + (CopyOfOriginalBitmap.GetPixel(j, i).B * 0.11));

                    CopyOfOriginalBitmap.SetPixel(j, i, Color.FromArgb(gray, gray, gray));
                }
            }
        }

        public void ChangePictureToNegative()
        {
            for (int i = 0; i < CopyOfOriginalBitmap.Height; i++)
            {
                for (int j = 0; j < CopyOfOriginalBitmap.Width; j++)
                {
                    CopyOfOriginalBitmap.SetPixel(j, i, Color.FromArgb(255, 255 - CopyOfOriginalBitmap.GetPixel(j, i).R, 255 - CopyOfOriginalBitmap.GetPixel(j, i).G, 255 - CopyOfOriginalBitmap.GetPixel(j, i).B));
                }
            }
        }

        public void ChangePictureToMirrorPicture()
        {
            for (int i = 1; i < CopyOfOriginalBitmap.Height; i++)
            {
                for (int j = 0, k = CopyOfOriginalBitmap.Width - 1; j < CopyOfOriginalBitmap.Width; j++, k--)
                {
                    CopyOfOriginalBitmap.SetPixel(j, i, Color.FromArgb(255, CopyOfOriginalBitmap.GetPixel(j, i).R, CopyOfOriginalBitmap.GetPixel(j, i).G, CopyOfOriginalBitmap.GetPixel(j, i).B));
                    CopyOfOriginalBitmap.SetPixel(k, i, Color.FromArgb(255, CopyOfOriginalBitmap.GetPixel(j, i).R, CopyOfOriginalBitmap.GetPixel(j, i).G, CopyOfOriginalBitmap.GetPixel(j, i).B));
                }
            }
        }

        public void RotatePictureLeft()
        {
            CopyOfOriginalBitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);
        }

        public void RotatePictureRight()
        {
            CopyOfOriginalBitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
        }

        public void RandomPictureFilter()
        {
            if (Enum.TryParse(randomNumberBetweenFiltering.Next(1, 5).ToString(), out m_FilterOptionsType))
            {
                FilterOptionsType = m_FilterOptionsType;
            }
        }

        public void SetSavedFilePath(string i_FileNameURL)
        {
            PicturePathAfterEdit = i_FileNameURL;
        }
    }
}