﻿namespace A16_Ex01_Anton_305939050_Daniel_305698912
{
    public partial class FormPictureFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonLoadFromFile = new System.Windows.Forms.Button();
            this.buttonLoadProfileFromFacebook = new System.Windows.Forms.Button();
            this.buttonGo = new System.Windows.Forms.Button();
            this.radioButtonGrays = new System.Windows.Forms.RadioButton();
            this.radioButtonNegative = new System.Windows.Forms.RadioButton();
            this.buttonPostOnFacebook = new System.Windows.Forms.Button();
            this.pictureBoxLoadedPicture = new System.Windows.Forms.PictureBox();
            this.pictureBoxAfterPicture = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.radioButtonMirror = new System.Windows.Forms.RadioButton();
            this.radioButtonRotateLeft = new System.Windows.Forms.RadioButton();
            this.radioButtonRotateRight = new System.Windows.Forms.RadioButton();
            this.radioButtonRandom = new System.Windows.Forms.RadioButton();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonSaveFile = new System.Windows.Forms.Button();
            this.buttonChooseYourPictureAgain = new System.Windows.Forms.Button();
            this.richTextBoxPostOnFacebook = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoadedPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAfterPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLoadFromFile
            // 
            this.buttonLoadFromFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonLoadFromFile.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLoadFromFile.Location = new System.Drawing.Point(9, 7);
            this.buttonLoadFromFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonLoadFromFile.Name = "buttonLoadFromFile";
            this.buttonLoadFromFile.Size = new System.Drawing.Size(91, 37);
            this.buttonLoadFromFile.TabIndex = 0;
            this.buttonLoadFromFile.Text = "Load File";
            this.buttonLoadFromFile.UseVisualStyleBackColor = false;
            this.buttonLoadFromFile.Click += new System.EventHandler(this.buttonLoadFromFile_Click);
            // 
            // buttonLoadProfileFromFacebook
            // 
            this.buttonLoadProfileFromFacebook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonLoadProfileFromFacebook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLoadProfileFromFacebook.Location = new System.Drawing.Point(105, 7);
            this.buttonLoadProfileFromFacebook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonLoadProfileFromFacebook.Name = "buttonLoadProfileFromFacebook";
            this.buttonLoadProfileFromFacebook.Size = new System.Drawing.Size(212, 37);
            this.buttonLoadProfileFromFacebook.TabIndex = 1;
            this.buttonLoadProfileFromFacebook.Text = "Load Profile From Facebook";
            this.buttonLoadProfileFromFacebook.UseVisualStyleBackColor = false;
            this.buttonLoadProfileFromFacebook.Click += new System.EventHandler(this.buttonLoadFromFacebook_Click);
            // 
            // buttonGo
            // 
            this.buttonGo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonGo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonGo.Location = new System.Drawing.Point(343, 132);
            this.buttonGo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(291, 37);
            this.buttonGo.TabIndex = 3;
            this.buttonGo.Text = "Go!";
            this.buttonGo.UseVisualStyleBackColor = false;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // radioButtonGrays
            // 
            this.radioButtonGrays.AutoSize = true;
            this.radioButtonGrays.Location = new System.Drawing.Point(339, 50);
            this.radioButtonGrays.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonGrays.Name = "radioButtonGrays";
            this.radioButtonGrays.Size = new System.Drawing.Size(118, 21);
            this.radioButtonGrays.TabIndex = 4;
            this.radioButtonGrays.TabStop = true;
            this.radioButtonGrays.Text = "Grays (Defalt)";
            this.radioButtonGrays.UseVisualStyleBackColor = true;
            this.radioButtonGrays.CheckedChanged += new System.EventHandler(this.radioButtonGrays_CheckedChanged);
            // 
            // radioButtonNegative
            // 
            this.radioButtonNegative.AutoSize = true;
            this.radioButtonNegative.Location = new System.Drawing.Point(339, 105);
            this.radioButtonNegative.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonNegative.Name = "radioButtonNegative";
            this.radioButtonNegative.Size = new System.Drawing.Size(85, 21);
            this.radioButtonNegative.TabIndex = 5;
            this.radioButtonNegative.TabStop = true;
            this.radioButtonNegative.Text = "Negative";
            this.radioButtonNegative.UseVisualStyleBackColor = true;
            this.radioButtonNegative.CheckedChanged += new System.EventHandler(this.radioButtonNegative_CheckedChanged);
            // 
            // buttonPostOnFacebook
            // 
            this.buttonPostOnFacebook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonPostOnFacebook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPostOnFacebook.Location = new System.Drawing.Point(483, 229);
            this.buttonPostOnFacebook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonPostOnFacebook.Name = "buttonPostOnFacebook";
            this.buttonPostOnFacebook.Size = new System.Drawing.Size(149, 37);
            this.buttonPostOnFacebook.TabIndex = 6;
            this.buttonPostOnFacebook.Text = "Post On Facebook";
            this.buttonPostOnFacebook.UseVisualStyleBackColor = false;
            this.buttonPostOnFacebook.Click += new System.EventHandler(this.buttonPostOnFacebook_Click);
            // 
            // pictureBoxLoadedPicture
            // 
            this.pictureBoxLoadedPicture.Location = new System.Drawing.Point(9, 50);
            this.pictureBoxLoadedPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxLoadedPicture.Name = "pictureBoxLoadedPicture";
            this.pictureBoxLoadedPicture.Size = new System.Drawing.Size(308, 298);
            this.pictureBoxLoadedPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLoadedPicture.TabIndex = 2;
            this.pictureBoxLoadedPicture.TabStop = false;
            // 
            // pictureBoxAfterPicture
            // 
            this.pictureBoxAfterPicture.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.pictureBoxAfterPicture.Location = new System.Drawing.Point(667, 50);
            this.pictureBoxAfterPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxAfterPicture.Name = "pictureBoxAfterPicture";
            this.pictureBoxAfterPicture.Size = new System.Drawing.Size(308, 298);
            this.pictureBoxAfterPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxAfterPicture.TabIndex = 7;
            this.pictureBoxAfterPicture.TabStop = false;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // radioButtonMirror
            // 
            this.radioButtonMirror.AutoSize = true;
            this.radioButtonMirror.Location = new System.Drawing.Point(339, 78);
            this.radioButtonMirror.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonMirror.Name = "radioButtonMirror";
            this.radioButtonMirror.Size = new System.Drawing.Size(66, 21);
            this.radioButtonMirror.TabIndex = 8;
            this.radioButtonMirror.TabStop = true;
            this.radioButtonMirror.Text = "Mirror";
            this.radioButtonMirror.UseVisualStyleBackColor = true;
            this.radioButtonMirror.CheckedChanged += new System.EventHandler(this.radioButtonMirror_CheckedChanged);
            // 
            // radioButtonRotateLeft
            // 
            this.radioButtonRotateLeft.AutoSize = true;
            this.radioButtonRotateLeft.Location = new System.Drawing.Point(499, 50);
            this.radioButtonRotateLeft.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonRotateLeft.Name = "radioButtonRotateLeft";
            this.radioButtonRotateLeft.Size = new System.Drawing.Size(99, 21);
            this.radioButtonRotateLeft.TabIndex = 9;
            this.radioButtonRotateLeft.TabStop = true;
            this.radioButtonRotateLeft.Text = "Rotate Left";
            this.radioButtonRotateLeft.UseVisualStyleBackColor = true;
            this.radioButtonRotateLeft.CheckedChanged += new System.EventHandler(this.radioButtonRotateLeft_CheckedChanged);
            // 
            // radioButtonRotateRight
            // 
            this.radioButtonRotateRight.AutoSize = true;
            this.radioButtonRotateRight.Location = new System.Drawing.Point(499, 78);
            this.radioButtonRotateRight.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonRotateRight.Name = "radioButtonRotateRight";
            this.radioButtonRotateRight.Size = new System.Drawing.Size(108, 21);
            this.radioButtonRotateRight.TabIndex = 10;
            this.radioButtonRotateRight.TabStop = true;
            this.radioButtonRotateRight.Text = "Rotate Right";
            this.radioButtonRotateRight.UseVisualStyleBackColor = true;
            this.radioButtonRotateRight.CheckedChanged += new System.EventHandler(this.radioButtonRotateRight_CheckedChanged);
            // 
            // radioButtonRandom
            // 
            this.radioButtonRandom.AutoSize = true;
            this.radioButtonRandom.Location = new System.Drawing.Point(499, 105);
            this.radioButtonRandom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonRandom.Name = "radioButtonRandom";
            this.radioButtonRandom.Size = new System.Drawing.Size(82, 21);
            this.radioButtonRandom.TabIndex = 11;
            this.radioButtonRandom.TabStop = true;
            this.radioButtonRandom.Text = "Random";
            this.radioButtonRandom.UseVisualStyleBackColor = true;
            this.radioButtonRandom.CheckedChanged += new System.EventHandler(this.radioButtonRandom_CheckedChanged);
            // 
            // buttonReset
            // 
            this.buttonReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonReset.Location = new System.Drawing.Point(343, 309);
            this.buttonReset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(81, 37);
            this.buttonReset.TabIndex = 12;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonSaveFile
            // 
            this.buttonSaveFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonSaveFile.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSaveFile.Location = new System.Drawing.Point(667, 7);
            this.buttonSaveFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonSaveFile.Name = "buttonSaveFile";
            this.buttonSaveFile.Size = new System.Drawing.Size(308, 37);
            this.buttonSaveFile.TabIndex = 13;
            this.buttonSaveFile.Text = "Save File";
            this.buttonSaveFile.UseVisualStyleBackColor = false;
            this.buttonSaveFile.Click += new System.EventHandler(this.buttonSaveFile_Click);
            // 
            // buttonChooseYourPictureAgain
            // 
            this.buttonChooseYourPictureAgain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(138)))), ((int)(((byte)(182)))));
            this.buttonChooseYourPictureAgain.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonChooseYourPictureAgain.Location = new System.Drawing.Point(429, 309);
            this.buttonChooseYourPictureAgain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonChooseYourPictureAgain.Name = "buttonChooseYourPictureAgain";
            this.buttonChooseYourPictureAgain.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonChooseYourPictureAgain.Size = new System.Drawing.Size(204, 37);
            this.buttonChooseYourPictureAgain.TabIndex = 14;
            this.buttonChooseYourPictureAgain.Text = "Choose Your Picture Again";
            this.buttonChooseYourPictureAgain.UseVisualStyleBackColor = false;
            this.buttonChooseYourPictureAgain.Click += new System.EventHandler(this.buttonChooseYourPictureAgain_Click);
            // 
            // richTextBoxPostOnFacebook
            // 
            this.richTextBoxPostOnFacebook.Location = new System.Drawing.Point(344, 175);
            this.richTextBoxPostOnFacebook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBoxPostOnFacebook.Name = "richTextBoxPostOnFacebook";
            this.richTextBoxPostOnFacebook.Size = new System.Drawing.Size(289, 48);
            this.richTextBoxPostOnFacebook.TabIndex = 15;
            this.richTextBoxPostOnFacebook.Tag = string.Empty;
            this.richTextBoxPostOnFacebook.Text = "Please Enter Your Post (Optional)";
            this.richTextBoxPostOnFacebook.TextChanged += new System.EventHandler(this.radioButtonGrays_CheckedChanged);
            this.richTextBoxPostOnFacebook.MouseDown += new System.Windows.Forms.MouseEventHandler(this.richTextBoxPostOnFacebook_MouseDown);
            // 
            // FormPictureFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(208)))), ((int)(((byte)(225)))));
            this.ClientSize = new System.Drawing.Size(324, 50);
            this.Controls.Add(this.buttonLoadFromFile);
            this.Controls.Add(this.richTextBoxPostOnFacebook);
            this.Controls.Add(this.buttonChooseYourPictureAgain);
            this.Controls.Add(this.buttonSaveFile);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.radioButtonRandom);
            this.Controls.Add(this.radioButtonRotateRight);
            this.Controls.Add(this.radioButtonRotateLeft);
            this.Controls.Add(this.radioButtonMirror);
            this.Controls.Add(this.pictureBoxAfterPicture);
            this.Controls.Add(this.buttonPostOnFacebook);
            this.Controls.Add(this.radioButtonNegative);
            this.Controls.Add(this.radioButtonGrays);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.pictureBoxLoadedPicture);
            this.Controls.Add(this.buttonLoadProfileFromFacebook);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormPictureFilter";
            this.Text = "PictureFilter";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoadedPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAfterPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLoadFromFile;
        private System.Windows.Forms.Button buttonLoadProfileFromFacebook;
        private System.Windows.Forms.PictureBox pictureBoxLoadedPicture;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.RadioButton radioButtonGrays;
        private System.Windows.Forms.Button buttonPostOnFacebook;
        private System.Windows.Forms.PictureBox pictureBoxAfterPicture;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.RadioButton radioButtonNegative;
        private System.Windows.Forms.RadioButton radioButtonMirror;
        private System.Windows.Forms.RadioButton radioButtonRotateLeft;
        private System.Windows.Forms.RadioButton radioButtonRotateRight;
        private System.Windows.Forms.RadioButton radioButtonRandom;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonSaveFile;
        private System.Windows.Forms.Button buttonChooseYourPictureAgain;
        private System.Windows.Forms.RichTextBox richTextBoxPostOnFacebook;
    }
}