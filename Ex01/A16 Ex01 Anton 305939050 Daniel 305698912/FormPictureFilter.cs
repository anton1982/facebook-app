﻿using System;
using System.Drawing;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;

namespace A16_Ex01_Anton_305939050_Daniel_305698912
{
    public partial class FormPictureFilter : Form
    {
        public enum eFilterOptions
        {
            FilterGray = 1,
            FilterNegative,
            FilterMirror,
            FilterRotateLeft,
            FilterRotateRight,
            RandomBetweenFilters
        }

        private readonly string r_DialogFilterChoose = "Jpeg Files (.jpg)|*.jpg|Bitmap Files (.bmp)|*.bmp|Gif Files (.gif)|*.gif|PNG Files (.png)|*.png";
        private readonly int r_TimerForChangeWindowFromSize = 250;
        private readonly bool r_IsCheckedOrEnabled = true;
        private readonly Random r_RandomNumberBetweenFiltering;
        private User m_User;
        private Bitmap m_OriginalBitmap, m_CopyOfOriginalBitmap;
        private eFilterOptions m_FilterOptions;
        private string m_OriginalPicturePath;
        private string m_PicturePathAfterEdit;
        private string m_PostMessage;

        public FormPictureFilter()
        {
            InitializeComponent();
            r_RandomNumberBetweenFiltering = new Random();
        }

        public FormPictureFilter(User i_User)
            : this()
        {
            User = i_User;
            formInitializer();
        }

        public bool IsCheckedOrEnabled
        {
            get { return r_IsCheckedOrEnabled; }
        }

        public eFilterOptions FilterOptions
        {
            get { return m_FilterOptions; }
            set { m_FilterOptions = value; }
        }

        public int TimerForChangeWindowFromSize
        {
            get { return r_TimerForChangeWindowFromSize; }
        }

        public string OriginalPicturePath
        {
            get { return m_OriginalPicturePath; }
            set { m_OriginalPicturePath = value; }
        }

        public string PicturePathAfterEdit
        {
            get { return m_PicturePathAfterEdit; }
            set { m_PicturePathAfterEdit = value; }
        }

        public string PostMessage
        {
            get { return m_PostMessage; }
            set { m_PostMessage = value; }
        }

        public string DialogFilterChoose
        {
            get { return r_DialogFilterChoose; }
        }

        public Random RandomNumberBetweenFiltering
        {
            get { return r_RandomNumberBetweenFiltering; }
        }

        public Bitmap OriginalBitmap
        {
            get { return m_OriginalBitmap; }
            set { m_OriginalBitmap = value; }
        }

        public Bitmap CopyOfOriginalBitmap
        {
            get { return m_CopyOfOriginalBitmap; }
            set { m_CopyOfOriginalBitmap = value; }
        }

        public User User
        {
            get { return m_User; }
            set { m_User = value; }
        }

        private void formInitializer()
        {
            setRadioButtonsEnableCheckState(!IsCheckedOrEnabled);
            setButtonsPressedState(IsCheckedOrEnabled);
            setRichTextBoxPostOnFacebookToDefaultText();
            pictureBoxImageRemover();
        }

        private void setButtonsPressedState(bool i_IsPressed)
        {
            setButtonLoadFromFileEnableState(i_IsPressed);
            setbuttonLoadProfileFromFacebookEnableState(i_IsPressed);
            setButtonGoEnableState(!i_IsPressed);
            setRichTextBoxPostOnFacebookEnableState(!i_IsPressed);
            setButtonPostOnFacebookEnableState(!i_IsPressed);
            setButtonChooseYourPictureAgainEnableState(!i_IsPressed);
            setButtonResetEnableState(!i_IsPressed);
            setButtonSaveFileEnableState(!i_IsPressed);
        }

        private void setRadioButtonsEnableCheckState(bool i_IsCheckedEnable)
        {
            radioButtonGrays.Checked = i_IsCheckedEnable;
            radioButtonGrays.Enabled = i_IsCheckedEnable;
            radioButtonNegative.Enabled = i_IsCheckedEnable;
            radioButtonMirror.Enabled = i_IsCheckedEnable;
            radioButtonRotateLeft.Enabled = i_IsCheckedEnable;
            radioButtonRotateRight.Enabled = i_IsCheckedEnable;
            radioButtonRandom.Enabled = i_IsCheckedEnable;
        }

        private void setButtonGoEnableState(bool i_IsEnabled)
        {
            buttonGo.Enabled = i_IsEnabled;
        }

        private void setButtonPostOnFacebookEnableState(bool i_IsEnabled)
        {
            buttonPostOnFacebook.Enabled = i_IsEnabled;
        }

        private void setButtonChooseYourPictureAgainEnableState(bool i_IsEnabled)
        {
            buttonChooseYourPictureAgain.Enabled = i_IsEnabled;
        }

        private void setButtonLoadFromFileEnableState(bool i_IsEnabled)
        {
            buttonLoadFromFile.Enabled = i_IsEnabled;
        }

        private void setbuttonLoadProfileFromFacebookEnableState(bool i_IsEnabled)
        {
            buttonLoadProfileFromFacebook.Enabled = i_IsEnabled;
        }

        private void setButtonSaveFileEnableState(bool i_IsEnabled)
        {
            buttonSaveFile.Enabled = i_IsEnabled;
        }

        private void setButtonResetEnableState(bool i_IsEnabled)
        {
            buttonReset.Enabled = i_IsEnabled;
        }

        private void setRichTextBoxPostOnFacebookEnableState(bool i_IsEnabled)
        {
            richTextBoxPostOnFacebook.Enabled = i_IsEnabled;
        }

        private void setRichTextBoxPostOnFacebookToDefaultText()
        {
            richTextBoxPostOnFacebook.Text = @"Please Enter Your Post (Optional)";
        }

        private void setProportyAfterLoadButtonsPressed()
        {
            setButtonsPressedState(!IsCheckedOrEnabled);
            setRadioButtonsEnableCheckState(IsCheckedOrEnabled);
            assignOriginalPictureInPictureBox();
            changeWindowsFormSize();
        }

        private void pictureBoxImageRemover()
        {
            pictureBoxLoadedPicture.Image = null;
            pictureBoxAfterPicture.Image = null;
        }

        private void assignOriginalPictureInPictureBox()
        {
            try
            {
                pictureBoxImageRemover();

                pictureBoxLoadedPicture.Load(OriginalPicturePath);
                pictureBoxAfterPicture.Load(OriginalPicturePath);

                if (OriginalBitmap == null || CopyOfOriginalBitmap == null)
                {
                    OriginalBitmap = new Bitmap(pictureBoxLoadedPicture.Image);
                    CopyOfOriginalBitmap = new Bitmap(pictureBoxLoadedPicture.Image);
                }
                else
                {
                    OriginalBitmap = pictureBoxLoadedPicture.Image as Bitmap;
                    CopyOfOriginalBitmap = pictureBoxLoadedPicture.Image.Clone() as Bitmap;
                }
            }
            catch (Exception ex)
            {   
                throw new Exception(ex.Message);
            }
        }

        private void assignFilteredPictureInPictureBox(Bitmap i_PictureInPictureBox)
        {
            pictureBoxLoadedPicture.Refresh();
            pictureBoxAfterPicture.Image = i_PictureInPictureBox;
        }
        
        private void changeWindowsFormSize()
        {
            for (int i = 0; i < TimerForChangeWindowFromSize; i++)
            {
                Width = Width + 2;
                Height++;
            }
        }

        private void changeWindowFormSizeToDefault()
        {
            for (int i = 0; i < TimerForChangeWindowFromSize; i++)
            {
                Width = Width - 2;
                Height--;
            }
        }

        private void changePictureToGrays()
        {
            for (int i = 0; i < CopyOfOriginalBitmap.Height; i++)
            {
                for (int j = 0; j < CopyOfOriginalBitmap.Width; j++)
                {
                    int gray = (int)((CopyOfOriginalBitmap.GetPixel(j, i).R * 0.3) + (CopyOfOriginalBitmap.GetPixel(j, i).G * 0.59) + (CopyOfOriginalBitmap.GetPixel(j, i).B * 0.11));

                    CopyOfOriginalBitmap.SetPixel(j, i, Color.FromArgb(gray, gray, gray));
                }
            }

            assignFilteredPictureInPictureBox(CopyOfOriginalBitmap);
        }

        private void changePictureToNegative()
        {
            for (int i = 0; i < CopyOfOriginalBitmap.Height; i++)
            {
                for (int j = 0; j < CopyOfOriginalBitmap.Width; j++)
                {
                    CopyOfOriginalBitmap.SetPixel(j, i, Color.FromArgb(255, 255 - CopyOfOriginalBitmap.GetPixel(j, i).R, 255 - CopyOfOriginalBitmap.GetPixel(j, i).G, 255 - CopyOfOriginalBitmap.GetPixel(j, i).B));
                }
            }

            assignFilteredPictureInPictureBox(CopyOfOriginalBitmap);
        }

        private void changePictureToMirrorPicture()
        {
            for (int i = 1; i < CopyOfOriginalBitmap.Height; i++)
            {
                for (int j = 0, k = CopyOfOriginalBitmap.Width - 1; j < CopyOfOriginalBitmap.Width; j++, k--)
                {
                    CopyOfOriginalBitmap.SetPixel(j, i, Color.FromArgb(255, CopyOfOriginalBitmap.GetPixel(j, i).R, CopyOfOriginalBitmap.GetPixel(j, i).G, CopyOfOriginalBitmap.GetPixel(j, i).B));
                    CopyOfOriginalBitmap.SetPixel(k, i, Color.FromArgb(255, CopyOfOriginalBitmap.GetPixel(j, i).R, CopyOfOriginalBitmap.GetPixel(j, i).G, CopyOfOriginalBitmap.GetPixel(j, i).B));
                }
            }

            assignFilteredPictureInPictureBox(CopyOfOriginalBitmap);
        }

        private void rotatePictureLeft()
        {
            CopyOfOriginalBitmap.RotateFlip(RotateFlipType.Rotate270FlipNone);

            assignFilteredPictureInPictureBox(CopyOfOriginalBitmap);
        }

        private void rotatePictureRight()
        {
            CopyOfOriginalBitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);

            assignFilteredPictureInPictureBox(CopyOfOriginalBitmap);
        }

        private void randomPictureFilter()
        {
            if (Enum.TryParse(RandomNumberBetweenFiltering.Next(1, 5).ToString(), out m_FilterOptions))
            {
                buttonGo.PerformClick();
                FilterOptions = eFilterOptions.RandomBetweenFilters;
            }
        }

        private void radioButtonGrays_CheckedChanged(object sender, EventArgs e)
        {
            FilterOptions = eFilterOptions.FilterGray;
        }

        private void radioButtonMirror_CheckedChanged(object sender, EventArgs e)
        {
            FilterOptions = eFilterOptions.FilterMirror;
        }

        private void radioButtonNegative_CheckedChanged(object sender, EventArgs e)
        {
            FilterOptions = eFilterOptions.FilterNegative;
        }

        private void radioButtonRotateLeft_CheckedChanged(object sender, EventArgs e)
        {
            FilterOptions = eFilterOptions.FilterRotateLeft;
        }

        private void radioButtonRotateRight_CheckedChanged(object sender, EventArgs e)
        {
            FilterOptions = eFilterOptions.FilterRotateRight;
        }

        private void radioButtonRandom_CheckedChanged(object sender, EventArgs e)
        {
            FilterOptions = eFilterOptions.RandomBetweenFilters;
        }
        
        private void buttonPostOnFacebook_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileHiddenFromUser("bitmapAfterEdit.jpg");
                postMessaegeFromUser();
                User.PostPhoto(PicturePathAfterEdit, PostMessage);

                messageToUser("Your Post Is On Facebook Now.");
            }
            catch (Exception)
            {
                messageToUser("Your Post Is Not On Facebook.");
            }
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            assignOriginalPictureInPictureBox();
            setRichTextBoxPostOnFacebookToDefaultText();
            setRadioButtonsEnableCheckState(IsCheckedOrEnabled);
        }

        private void buttonChooseYourPictureAgain_Click(object sender, EventArgs e)
        {
            formInitializer();
            changeWindowFormSizeToDefault();
        }

        private void buttonSaveFile_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog.Filter = DialogFilterChoose;
                saveFileDialog.FileName = "Enter File Name";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    pictureBoxAfterPicture.Image.Save(saveFileDialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    messageToUser("Your picture was successfully saved.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show(@"Your picture was not saved.");
            }
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {
            switch (FilterOptions)
            {
                case eFilterOptions.FilterGray:
                    {
                        changePictureToGrays();
                        break;
                    }

                case eFilterOptions.FilterNegative:
                    {
                        changePictureToNegative();
                        break;
                    }

                case eFilterOptions.FilterMirror:
                    {
                        changePictureToMirrorPicture();
                        break;
                    }

                case eFilterOptions.FilterRotateLeft:
                    {
                        rotatePictureLeft();
                        break;
                    }

                case eFilterOptions.FilterRotateRight:
                    {
                        rotatePictureRight();
                        break;
                    }

                case eFilterOptions.RandomBetweenFilters:
                    {
                        randomPictureFilter();
                        break;
                    }

                default:
                    {
                        MessageBox.Show(@"Please choose an option.");
                        break;
                    }
            }
        }

        private void buttonLoadFromFile_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog.Filter = DialogFilterChoose;
                openFileDialog.FileName = "Select File";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    OriginalPicturePath = openFileDialog.FileName;
                    setProportyAfterLoadButtonsPressed();
                }
            }
            catch (Exception)
            {
                MessageBox.Show(@"Picture was not choosen.");
            }
        }

        private void buttonLoadFromFacebook_Click(object sender, EventArgs e)
        {
            try
            {
                OriginalPicturePath = User.PictureLargeURL;

                setProportyAfterLoadButtonsPressed();
            }
            catch (Exception ex)
            {
                messageToUser(ex.Message);
            }
        }

        private void richTextBoxPostOnFacebook_MouseDown(object sender, EventArgs e)
        {
            richTextBoxPostOnFacebook.Text = string.Empty;
        }

        private void saveFileHiddenFromUser(string i_FileName)
        {
            try
            {
                pictureBoxAfterPicture.Image.Save(AppDomain.CurrentDomain.BaseDirectory + "\\" + i_FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                PicturePathAfterEdit = AppDomain.CurrentDomain.BaseDirectory + "\\" + i_FileName;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void postMessaegeFromUser()
        {
            string defaultPost = "Please Enter Your Post (Optional)";
            PostMessage = string.Empty;

            if (!richTextBoxPostOnFacebook.Equals(defaultPost))
            {
                PostMessage = richTextBoxPostOnFacebook.Text;
            }
        }

        private void messageToUser(string i_MsgToUser)
        {
            MessageBox.Show(i_MsgToUser);
        }
    }
}