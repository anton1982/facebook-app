﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace A16_Ex01_Anton_305939050_Daniel_305698912
{
    public class AppConfig
    {
        private static AppConfig s_AppConfig = null;

        public static AppConfig GetOrCreate()
        {
            if (s_AppConfig == null)
            {
                    if (s_AppConfig == null)
                    {
                        s_AppConfig = new AppConfig();
                    }
            }

            return s_AppConfig;
        }

        public static void ReadFromFile()
        {
            if (File.Exists("AppConfig.xml"))
            {
                Stream stream = new FileStream("AppConfig.xml", FileMode.Open);
                XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
                s_AppConfig = serializer.Deserialize(stream) as AppConfig;
                stream.Dispose();
            }
        }

        public string AccessToken { get; set; }
        
        public Point Location { get; set; }

        public bool RememberMe { get; set; }

        public void SaveToFile()
        {
            Stream stream = new FileStream("AppConfig.xml", FileMode.Create);
            XmlSerializer serializer = new XmlSerializer(typeof(AppConfig));
            serializer.Serialize(stream, this);
            stream.Dispose();
        }
    }
}
