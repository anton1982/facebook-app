﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace A16_Ex01_Anton_305939050_Daniel_305698912
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormFacebookMain());
        }
    }
}
