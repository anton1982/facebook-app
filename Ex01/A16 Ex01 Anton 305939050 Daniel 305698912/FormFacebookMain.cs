﻿using System;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;
using FacebookWrapper;

namespace A16_Ex01_Anton_305939050_Daniel_305698912
{
    public partial class FormFacebookMain : Form
    {
        public enum eAccountStatus
        {
            Loggedin = 1,
            Loggedout
        }

        private const string k_AppID = "772224062883316";
        private const string k_DefaultPost = "Please Enter Your Post (Optional)";
        private static eAccountStatus s_AccountLoggedIn;
        private readonly bool r_IsCheckedOrEnabled = true;
        private User m_LoggedInUser;
        private AppConfig m_AppConfig;
        private FormPictureFilter m_FormPictureFilter;
        private bool m_IsSearchByDateEnabled = false;
        private string m_AccessToken;

        public static eAccountStatus AccountLoggedIn
        {
            get { return s_AccountLoggedIn; }
            set { s_AccountLoggedIn = value; }
        }

        public User LoggedInUser
        {
            get { return m_LoggedInUser; }
            set { m_LoggedInUser = value; }
        }

        public bool IsSearchByDateEnabled
        {
            get { return m_IsSearchByDateEnabled; }
            set { m_IsSearchByDateEnabled = value; }
        }

        public bool IsCheckedOrEnabled
        {
            get { return r_IsCheckedOrEnabled; }
        }

        public string AccessToken
        {
            get { return m_AccessToken; }
            set { m_AccessToken = value; }
        }

        public FormFacebookMain()
        {
            InitializeComponent();
            AccountLoggedIn = eAccountStatus.Loggedout;
        }

        protected override void OnLoad(EventArgs e)
        {
            AppConfig.ReadFromFile();
            m_AppConfig = AppConfig.GetOrCreate();
            this.m_AccessToken = m_AppConfig.AccessToken;
            this.Location = m_AppConfig.Location;
            checkBoxAutoConnect.Checked = m_AppConfig.RememberMe;
            resetPropertiesForm();

            if (checkBoxAutoConnect.Checked)
            {
                try
                {
                    connect(FacebookService.Connect(m_AppConfig.AccessToken));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format(
@"{0}
{1}",
                        ex.Message,
                        ex.StackTrace));
                }
            }

            base.OnLoad(e);
        }

        public string AppId
        {
            get { return k_AppID; }
        }

        private void buttonLoginOrLogout_Click(object sender, EventArgs e)
        {
            if (AccountLoggedIn == eAccountStatus.Loggedout)
            {
                loginAndInit();
            }
            else
            {
                logout();
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            m_AppConfig.AccessToken = AccessToken;
            m_AppConfig.Location = this.Location;
            m_AppConfig.RememberMe = this.checkBoxAutoConnect.Checked;
            m_AppConfig.SaveToFile();

            base.OnClosing(e);
        }

        private void loginAndInit()
        {
            try
            {
                LoginResult result = FacebookWrapper.FacebookService.Login(
                    AppId,
                    "user_about_me",
                    "user_friends",
                    "user_posts",
                    "user_events",
                    "user_photos",
                    "publish_stream",
                    "publish_actions",
                    "user_status");

                connect(result);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void logout()
        {
            LoggedInUser = null;
            AccountLoggedIn = eAccountStatus.Loggedout;
            FacebookService.Logout(null);
            AccessToken = null;
            buttonLogin.Text = "Login";
            resetPropertiesForm();
            messageToUser("You have been disconected");
        }

        private void resetPropertiesForm()
        {
            setButtonsAndBoxsPressedEnbaledState(!IsCheckedOrEnabled);
            clearBoxsInformation();
        }

        private void setButtonsAndBoxsPressedEnbaledState(bool i_IsPressedOrEnabled)
        {
            buttonPictureFilter.Enabled = i_IsPressedOrEnabled;
            buttonConnect.Enabled = !i_IsPressedOrEnabled;
            buttonReset.Enabled = i_IsPressedOrEnabled;
            buttonPostOnFacebook.Enabled = i_IsPressedOrEnabled;

            checkBoxAutoConnect.Enabled = i_IsPressedOrEnabled;

            richTextBoxPostOnFacebook.Enabled = i_IsPressedOrEnabled;

            textBoxSearchOrPost.Enabled = i_IsPressedOrEnabled;

            checkBoxIfDateWasChecked.Enabled = i_IsPressedOrEnabled;
        }

        private void clearBoxsInformation()
        {
            AccountLoggedIn = eAccountStatus.Loggedout;

            userProfilePictureBox.Image = userProfilePictureBox.ErrorImage;

            this.labelUsername.Text = string.Empty;
            this.labelBirthday.Text = string.Empty;
            this.labelGender.Text = string.Empty;

            richTextBoxPostOnFacebook.Text = k_DefaultPost;
            textBoxSearchOrPost.Text = string.Empty;

            dataGridViewCheckIn.Rows.Clear();
            dataGridViewEvents.Rows.Clear();
            dataGridViewPost.Rows.Clear();
            dataGridViewFriends.Rows.Clear();
        }

        private void connect(LoginResult i_Result)
        {
            if (!string.IsNullOrEmpty(i_Result.AccessToken))
            {
                LoggedInUser = i_Result.LoggedInUser;
                AccountLoggedIn = eAccountStatus.Loggedin;
                buttonLogin.Text = "Logout";
                m_AccessToken = i_Result.AccessToken;
                setButtonsAndBoxsPressedEnbaledState(IsCheckedOrEnabled);
                getUserInfo();
            }
            else
            {
                messageToUser(i_Result.ErrorMessage);
            }
        }

        private void getUserInfo()
        {
            try
            {
                this.userProfilePictureBox.LoadAsync(LoggedInUser.PictureLargeURL);
                this.labelUsername.Text = "Name: " + LoggedInUser.Name;
                this.labelBirthday.Text = "Birthday: " + LoggedInUser.Birthday;
                this.labelGender.Text = "Gender: " + LoggedInUser.Gender;

                getProfilePosts();
                getEventsDetails();
                getCheckInDetails();
                getFriendList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void getFriendList()
        {
            dataGridViewFriends.Rows.Clear();

            foreach (User facebookFriend in LoggedInUser.Friends)
            {
                dataGridViewFriends.Rows.Add(facebookFriend, facebookFriend.Name, facebookFriend.UpdateTime);
            }
        }

        private void getCheckInDetails()
        {
            dataGridViewCheckIn.Rows.Clear();

            foreach (Checkin facebookCheckin in LoggedInUser.Checkins)
            {
                dataGridViewCheckIn.Rows.Add(facebookCheckin, facebookCheckin.Place.Name, facebookCheckin.Message, facebookCheckin.LikedBy.Count);
            }
        }

        private void getEventsDetails()
        {
            dataGridViewEvents.Rows.Clear();

            foreach (Event facebookEvent in LoggedInUser.Events)
            {
                dataGridViewEvents.Rows.Add(
                    facebookEvent, 
                    facebookEvent.Name, 
                    facebookEvent.StartTime,
                    facebookEvent.Description);
            }
        }

        private void getProfilePosts()
        {
            dataGridViewPost.Rows.Clear();

            foreach (Post post in LoggedInUser.Posts)
            {
                if (!string.IsNullOrEmpty(post.Message))
                {
                    dataGridViewPost.Rows.Add(post, post.Message, post.UpdateTime);
                } 
                else if (!string.IsNullOrEmpty(post.Caption))
                {
                    dataGridViewPost.Rows.Add(post, post.Caption);
                }
                else
                {
                    StringBuilder postType = new StringBuilder(post.Type.ToString());
                    postType[0] = char.ToUpper(postType[0]);
                    dataGridViewPost.Rows.Add(post, string.Format("[{0}]", postType));
                }
            }

            if (LoggedInUser.Posts.Count == 0)
            {
                messageToUser("No Posts to retrieve.");
            }
        }

        private void userProfileInfo_Enter(object sender, EventArgs e)
        {
        }

        private void userProfilePictureBox_Click(object sender, EventArgs e)
        {
        }

        private void formFacebookMain_Load(object sender, EventArgs e)
        {
        }

        private void searchWordsInPosts()
        {
            DateTime dateFormat;
            bool isSearchSucceded = false;

            dataGridViewPost.Rows.Clear();

            foreach (Post facbookPost in LoggedInUser.Posts)
            {
                if (!string.IsNullOrEmpty(facbookPost.Message) && facbookPost.Message.Contains(textBoxSearchOrPost.Text))
                {
                    isSearchSucceded = true;

                    if (IsSearchByDateEnabled)
                    {
                        try
                        {
                            if (DateTime.TryParse(facbookPost.UpdateTime.ToString(), out dateFormat))
                            {
                                isSearchSucceded = searchByDate(dateFormat);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }

                    if (isSearchSucceded)
                    {
                        dataGridViewPost.Rows.Add(facbookPost, facbookPost.Message, facbookPost.UpdateTime);
                    }
                }
            }
        }

        private void searchWordsInEvents()
        {
            DateTime dateFormat;
            bool isSearchSucceded = false;

            dataGridViewEvents.Rows.Clear();

            foreach (Event facbookEvent in LoggedInUser.Events)
            {
                if (!string.IsNullOrEmpty(facbookEvent.Name) && facbookEvent.Name.Contains(textBoxSearchOrPost.Text))
                {
                    isSearchSucceded = true;

                    if (IsSearchByDateEnabled)
                    {
                        try
                        {
                            if (DateTime.TryParse(facbookEvent.StartTime.ToString(), out dateFormat))
                            {
                                isSearchSucceded = searchByDate(dateFormat);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }

                    if (isSearchSucceded)
                    {
                        dataGridViewEvents.Rows.Add(
                            facbookEvent, 
                            facbookEvent.Name, 
                            facbookEvent.StartTime,
                            facbookEvent.Description);
                    }
                }
            }
        }

        private void searchWordsInCheckIn()
        {
            DateTime dateFormat;
            bool isSearchSucceded = false;

            dataGridViewCheckIn.Rows.Clear();

            foreach (Checkin facebookCheckin in LoggedInUser.Checkins)
            {
                if (!string.IsNullOrEmpty(facebookCheckin.Place.Name) && facebookCheckin.Place.Name.Contains(textBoxSearchOrPost.Text))
                {
                    isSearchSucceded = true;

                    if (IsSearchByDateEnabled)
                    {
                        try
                        {
                            if (DateTime.TryParse(facebookCheckin.UpdateTime.ToString(), out dateFormat))
                            {
                                isSearchSucceded = searchByDate(dateFormat);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }

                    if (isSearchSucceded)
                    {
                        dataGridViewCheckIn.Rows.Add(
                            facebookCheckin, 
                            facebookCheckin.Place.Name, 
                            facebookCheckin.Message,
                            facebookCheckin.LikedBy.Count);
                    }
                }
            }
        }

        private void searchWordsInFriends()
        {
            DateTime dateFormat;
            bool isSearchSucceded = false;

            dataGridViewFriends.Rows.Clear();

            foreach (User facebookFriend in LoggedInUser.Friends)
            {
                if (!string.IsNullOrEmpty(facebookFriend.Name) && facebookFriend.Name.Contains(textBoxSearchOrPost.Text))
                {
                    isSearchSucceded = true;

                    if (IsSearchByDateEnabled)
                    {
                        try
                        {
                            if (DateTime.TryParse(facebookFriend.UpdateTime.ToString(), out dateFormat))
                            {
                                isSearchSucceded = searchByDate(dateFormat);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }

                    if (isSearchSucceded)
                    {
                        dataGridViewFriends.Rows.Add(facebookFriend, facebookFriend.Name, facebookFriend.UpdateTime);
                    }
                }
            }
        }

        private bool searchByDate(DateTime i_FaceBookDateTimeProperty)
        {
            bool isSearchSucceded = false;
            DateTime facebookUserDateTimeShortStartDateTime = new DateTime();
            DateTime facebookUserDateTimeShortEndDateTime = new DateTime();
            DateTime facebookDateTimeProperty = new DateTime();

            if (DateTime.TryParse(i_FaceBookDateTimeProperty.ToShortDateString(), out facebookDateTimeProperty))
            {
                if (DateTime.TryParse(
                    dateTimePickerForSearchStartDate.Value.ToShortDateString(),
                    out facebookUserDateTimeShortStartDateTime) 
                    && DateTime.TryParse(
                    dateTimePickerForSearchEndDate.Value.ToShortDateString(),
                    out facebookUserDateTimeShortEndDateTime))
                {
                    if (facebookDateTimeProperty >= facebookUserDateTimeShortStartDateTime
                        && facebookDateTimeProperty <= facebookUserDateTimeShortEndDateTime)
                    {
                        isSearchSucceded = true;
                    }
                    else
                    {
                        isSearchSucceded = false;
                    }
                }
                else
                {
                    throw new Exception("You choose wrong date");
                }
            }
            else
            {
                throw new Exception("Date is facebook app at property is not correct");
            }

            return isSearchSucceded;
        }

        private void uploadPost()
        {
            try
            {
                if (!string.IsNullOrEmpty(richTextBoxPostOnFacebook.Text))
                {
                    LoggedInUser.PostStatus(richTextBoxPostOnFacebook.Text);
                }
                else
                {
                    messageToUser("Can't post empty message.");
                }
            }
            catch (Exception)
            {
                messageToUser("Your Post Is Not On Facebook.");
            }
        }

        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (AccountLoggedIn == eAccountStatus.Loggedin)
            {
                MessageBox.Show("You are already connected to faceboook");
            }
            else if (string.IsNullOrEmpty(m_AccessToken))
            {
                MessageBox.Show("No access token available", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                AppConfig.ReadFromFile();
                m_AppConfig = AppConfig.GetOrCreate();
                this.m_AccessToken = m_AppConfig.AccessToken;
                LoginResult result = FacebookService.Connect(m_AppConfig.AccessToken);

                connect(result);
            }
        }

        private void buttonPostOnFacebook_Click(object sender, EventArgs e)
        {
            uploadPost();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            if (tabPosts.Visible)
            {
                getProfilePosts();
            }
            else if (tabEvents.Visible)
            {
                getEventsDetails();
            }
            else if (tabCheckIn.Visible)
            {
                getCheckInDetails();
            }
            else if (tabFriends.Visible)
            {
                getFriendList();
            }

            checkBoxIfDateWasChecked.Checked = false;
            textBoxSearchOrPost.Text = string.Empty;
            richTextBoxPostOnFacebook.Text = k_DefaultPost;
        }

        private void searchWords()
        {
            if (tabPosts.Visible)
            {
                searchWordsInPosts();
            }
            else if (tabEvents.Visible)
            {
                searchWordsInEvents();
            }
            else if (tabCheckIn.Visible)
            {
                searchWordsInCheckIn();
            }
            else if (tabFriends.Visible)
            {
                searchWordsInFriends();
            }
        }

        private void dataGridViewPost_SelectionChanged(object sender, EventArgs e)
        {
            pictureBoxAttachedPhoto.Visible = true;
            labelAttachedPhoto.Visible = true;

            DataGridView dataGridView = sender as DataGridView;
            if (dataGridView != null)
            {
                if (dataGridView.CurrentRow != null)
                {
                    Post userPost = dataGridView.CurrentRow.Cells[0].Value as Post;

                    if (userPost != null && userPost.PictureURL != null)
                    {
                        pictureBoxAttachedPhoto.LoadAsync(userPost.PictureURL);
                    }
                    else
                    {
                        pictureBoxAttachedPhoto.Image = pictureBoxAttachedPhoto.ErrorImage;
                    }
                }
            }
        }

        private void dataGridViewEvents_SelectionChanged(object sender, EventArgs e)
        {
            pictureBoxAttachedPhoto.Visible = true;
            labelAttachedPhoto.Visible = true;

            DataGridView dataGridView = sender as DataGridView;
            if (dataGridView != null)
            {
                if (dataGridView.CurrentRow != null)
                {
                    Event userEvent = dataGridView.CurrentRow.Cells[0].Value as Event;

                    if (userEvent != null && userEvent.PictureNormalURL != null)
                    {
                        pictureBoxAttachedPhoto.LoadAsync(userEvent.PictureLargeURL);
                    }
                    else
                    {
                        pictureBoxAttachedPhoto.Image = pictureBoxAttachedPhoto.ErrorImage;
                    }
                }
            }
        }

        private void dataGridViewCheckIn_SelectionChanged(object sender, EventArgs e)
        {
            pictureBoxAttachedPhoto.Visible = false;
            labelAttachedPhoto.Visible = false;
        }

        private void dataGridViewFriends_SelectionChanged(object sender, EventArgs e)
        {
            pictureBoxAttachedPhoto.Visible = true;
            labelAttachedPhoto.Visible = true;

            DataGridView dataGridView = sender as DataGridView;
            if (dataGridView != null)
            {
                if (dataGridView.CurrentRow != null)
                {
                    User user = dataGridView.CurrentRow.Cells[0].Value as User;

                    if (user != null && user.PictureNormalURL != null)
                    {
                        pictureBoxAttachedPhoto.LoadAsync(user.PictureLargeURL);
                    }
                    else
                    {
                        pictureBoxAttachedPhoto.Image = pictureBoxAttachedPhoto.ErrorImage;
                    }
                }
            }
        }

        private void textBoxSearchOrPost_TextChanged(object sender, EventArgs e)
        {
            searchWords();
        }

        private void FormFacebookMain_MouseClick(object sender, MouseEventArgs e)
        {
            richTextBoxPostOnFacebook.Text = k_DefaultPost;
        }

        private void richTextBoxPostOnFacebook_MouseDown(object sender, EventArgs e)
        {
            richTextBoxPostOnFacebook.Text = string.Empty;
        }

        private void checkBoxIfDateWasChecked_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxIfDateWasChecked.Checked)
            {
                IsSearchByDateEnabled = true;
                dateTimePickerForSearchStartDate.Enabled = true;
                dateTimePickerForSearchEndDate.Enabled = true;
                
                searchWords();
            }
            else
            {
                IsSearchByDateEnabled = false;
                dateTimePickerForSearchStartDate.Enabled = false;
                dateTimePickerForSearchEndDate.Enabled = false;

                searchWords();
            }
        }
        
        private void messageToUser(string i_MessageToUser)
        {
            MessageBox.Show(i_MessageToUser);
        }

        private void buttonPictureFilter_Click(object sender, EventArgs e)
        {
            m_FormPictureFilter = new FormPictureFilter(LoggedInUser);
            m_FormPictureFilter.ShowDialog();
        }

        private void dateTimePickerForSearchStartDate_ValueChanged(object sender, EventArgs e)
        {
            searchWords();
        }

        private void dateTimePickerForSearchEndDate_ValueChanged(object sender, EventArgs e)
        {
            searchWords();
        }
    }
}